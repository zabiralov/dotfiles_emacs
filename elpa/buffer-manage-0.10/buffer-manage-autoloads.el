;;; buffer-manage-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "buffer-manage" "buffer-manage.el" (0 0 0 0))
;;; Generated autoloads from buffer-manage.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "buffer-manage" '("buffer-")))

;;;***

;;;### (autoloads nil "config-manage-base" "config-manage-base.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from config-manage-base.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "config-manage-base" '("config-")))

;;;***

;;;### (autoloads nil "config-manage-core" "config-manage-core.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from config-manage-core.el

(autoload 'config-manage-declare-functions "config-manage-core" "\
Declare functions in list FNS for the purposes of silencing the compiler.

This is used in the compiler module libraries to silence the compiler in
`eval-when-compile' scopes.

\(fn &rest FNS)" nil t)

(autoload 'config-manage-declare-methods "config-manage-core" "\
Declare methods in list FNS for the purposes of silencing the compiler.

This is used in the compiler module libraries to silence the compiler in
`eval-when-compile' scopes.

\(fn &rest FNS)" nil t)

(autoload 'config-manage-declare-variables "config-manage-core" "\
Declare variables in list VARS for the purposes of silencing the compiler.

This is used in the compiler module libraries to silence the compiler in
`eval-when-compile' scopes.

\(fn &rest VARS)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "config-manage-core" '("config-manage-")))

;;;***

;;;### (autoloads nil "config-manage-mode" "config-manage-mode.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from config-manage-mode.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "config-manage-mode" '("config-manage-")))

;;;***

;;;### (autoloads nil "config-manage-prop" "config-manage-prop.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from config-manage-prop.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "config-manage-prop" '("config-")))

;;;***

;;;### (autoloads nil nil ("buffer-manage-pkg.el" "config-manage.el")
;;;;;;  (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; buffer-manage-autoloads.el ends here
