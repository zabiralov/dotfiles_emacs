;;; -*- no-byte-compile: t -*-
(define-package "treemacs-magit" "2.6" "Magit integration for treemacs" '((emacs "25.2") (treemacs "0.0") (pfuture "1.3") (magit "2.90.0")) :commit "e01ad21ea3cc0eba8cd460737116b51be32ffb45" :authors '(("Alexander Miller" . "alexanderm@web.de")) :maintainer '("Alexander Miller" . "alexanderm@web.de") :url "https://github.com/Alexander-Miller/treemacs")
