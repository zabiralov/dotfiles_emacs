;ELC   
;;; Compiled
;;; in Emacs version 26.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(require 'cl-lib)
#@13 CIDR value.
(defconst ipcalc--cidr-default 32 (#$ . 426))
#@54 Convert integer N to bit string (LENGTH, default 8).
(defalias 'ipcalc-int-to-bin-string #[(n &optional length) "\305	\206 \306\307\n\310\"\nW\203. \311\f\312\313\"\"\305U\204' \nTZ\314I\210T\211\202 +\207" [i length len s n 0 8 make-string 48 logand ash 1 49] 6 (#$ . 490)])
#@50 Return LIST-OF-OCTETS as a single binary string.
(defalias 'ipcalc-octets-as-binary #[(list-of-octets) "\302	\203 \303\304	@!!P	A\211\204 )\207" [binary list-of-octets "" ipcalc-int-to-bin-string string-to-number] 5 (#$ . 784)])
#@41 Split IP address and return the octets.
(defalias 'ipcalc-ip-to-octets #[(ip) "\302\303\"\211G\304U\203 	\202 \305\306!)\207" [ip octets split-string "\\." 4 message "not correct IP format"] 4 (#$ . 1026)])
#@52 Return 1's equal to NUM and pad the rest up to 32.
(defalias 'ipcalc-ones-and-pad #[(num) "	X\203\f \302V\204 \303\304!\210\305\306\"\305	Z\307\"P\207" [num ipcalc--cidr-default 0 error "Wrong value provided" make-string 49 48] 4 (#$ . 1243)])
#@129 Invert NUM's 1s to 0s and vice versa.

  (credit: the simpler version below re-writen by pjb from #emacs
          on freenode)
(defalias 'ipcalc-invert-binary #[(num) "\301\302\303\304\"\305\306\307$\205 \310\"\207" [num string #[(ch) "\301\302\303\"H\207" [ch "10" cl-position "01"] 4] cl-remove-if-not #[(ch) "\301\302\"\207" [ch cl-position "01"] 3] nil apply cl-mapcar cl-coerce] 9 (#$ . 1499)])
#@39 Takes IP & CIDR and produces network.
(defalias 'ipcalc-network #[(ip cidr) "\304!\305\306\n!!\307	O\310	Z\311\"P)\207" [cidr cidr-as-int ip ipcalc--cidr-default string-to-number ipcalc-octets-as-binary ipcalc-ip-to-octets 0 make-string 48] 4 (#$ . 1917)])
#@37 Increment the given BINARY-IP by 1.
(defalias 'ipcalc-host+1 #[(binary-ip) "\211\nS\303I\210	)\207" [binary-ip tmp ipcalc--cidr-default 49] 4 (#$ . 2183)])
#@59 Given IP and CIDR, return maximum host as a binary value.
(defalias 'ipcalc-host-max #[(ip cidr) "\305!	S\211\nW\203 \f\306I\210T\211\211\202 \f*\207" [cidr ipcalc--cidr-default max count ip string-to-number 49] 4 (#$ . 2347)])
#@40 Calculate Hosts/Net for the NUM given.
(defalias 'ipcalc-hosts/net #[(num) "\302\303	Z\"\303Z\207" [ipcalc--cidr-default num expt 2] 4 (#$ . 2590)])
#@38 Convert binary value BIN to integer.
(defalias 'ipcalc-bin-to-int #[(bin) "\301\302\303P!!\207" [bin int-to-string read "#b"] 4 (#$ . 2746)])
#@31 Convert BINARY to IP address.
(defalias 'ipcalc-binary-to-ip #[(binary) "\306\307\n\307\310O\n\310\311O\n\311\312O\n\312\313O\314\315\fF\"	\316W\203; 	8\317Q	T\211\211\202% \320!@P.\207" [full-ip count binary 1st-octet 2nd-octet 3rd-octet nil 0 8 16 24 32 mapcar ipcalc-bin-to-int 3 "." last 4th-octet octets] 7 (#$ . 2895)])
#@34 IP calculator for given IP/CIDR.
(defalias 'ipcalc #[(ip/cidr) "\306\307\"@\310\311	!!\306\307\"A@\312!\313\f!\314\313\f!!\315! \315\314!!!\316	\"\"\315\"!#\317\316	\"\"$\315$!%\320\316	\"!&\315&!'\320\317\"\"!(\315(!)\321*\322*!\203w \323*!\210\324*!\210\325\326	\n#\325\327!$\325\330 #\325\331#\316	\"#\325\332'&#\325\333%$#\325\334)(#\325\335\336\f!\"\261.\207" [ip/cidr ip ip-in-binary cidr cidr-int cidr-binary split-string "/" ipcalc-octets-as-binary ipcalc-ip-to-octets string-to-number ipcalc-ones-and-pad ipcalc-invert-binary ipcalc-binary-to-ip ipcalc-network ipcalc-host-max ipcalc-host+1 "*ipcalc*" get-buffer kill-buffer pop-to-buffer format "Address:%15s%41s\n" "Netmask:%16s = %2s %34s\n" "Wildcard:%11s%44s\n" "=>\nNetwork:%14s%42s\n" "HostMin:%14s%42s\n" "HostMax:%16s%40s\n" "Broadcast:%14s%40s\n" "Hosts/Net: %d\n" ipcalc-hosts/net wildcard-binary wildcard-ip netmask net-binary net-ip host-max-binary host-max-ip host-min-binary host-min-ip broadcast-binary broadcast-ip buffer] 11 (#$ . 3250) "sIP/CIDR: "])
(provide 'ipcalc)
