;;; -*- no-byte-compile: t -*-
(define-package "ipcalc" "20170926.805" "IP subnet calculator" '((cl-lib "0.5")) :commit "2720f7e3e662e04e195f8338b81a499cf321296a" :keywords '("networking" "tools") :authors '(("\"Aleksandar Simic\"" . "asimic@gmail.com")) :maintainer '("\"Aleksandar Simic\"" . "asimic@gmail.com") :url "http://github.com/dotemacs/ipcalc.el")
