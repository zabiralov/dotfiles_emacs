;;; -*- no-byte-compile: t -*-
(define-package "treemacs-icons-dired" "2.6" "Treemacs icons for dired" '((treemacs "0.0") (emacs "25.2") (cl-lib "0.5")) :commit "e01ad21ea3cc0eba8cd460737116b51be32ffb45" :authors '(("Alexander Miller" . "alexanderm@web.de")) :maintainer '("Alexander Miller" . "alexanderm@web.de") :url "https://github.com/Alexander-Miller/treemacs")
