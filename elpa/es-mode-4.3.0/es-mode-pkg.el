(define-package "es-mode" "4.3.0" "A major mode for editing Elasticsearch queries"
  '((dash "2.11.0")
    (cl-lib "0.5")
    (spark "1.0"))
  :keywords
  '("elasticsearch")
  :authors
  '(("Lee Hinman" . "lee@writequit.org"))
  :maintainer
  '("Lee Hinman" . "lee@writequit.org")
  :url "http://www.github.com/dakrone/es-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
