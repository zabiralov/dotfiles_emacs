;;; -*- no-byte-compile: t -*-
(define-package "eyebrowse" "0.7.8" "Easy window config switching" '((dash "2.7.0") (emacs "24.3.1")) :commit "d75e37a048718d6981c366c431b93ccbe884f356" :keywords '("convenience") :authors '(("Vasilij Schneidermann" . "v.schneidermann@gmail.com")) :maintainer '("Vasilij Schneidermann" . "v.schneidermann@gmail.com") :url "https://github.com/wasamasa/eyebrowse")
