(define-package "emacsql-psql" "3.0.0" "EmacSQL back-end for PostgreSQL via psql"
  '((emacs "25.1")
    (emacsql "2.0.0"))
  :authors
  '(("Christopher Wellons" . "wellons@nullprogram.com"))
  :maintainer
  '("Christopher Wellons" . "wellons@nullprogram.com")
  :url "https://github.com/skeeto/emacsql")
;; Local Variables:
;; no-byte-compile: t
;; End:
