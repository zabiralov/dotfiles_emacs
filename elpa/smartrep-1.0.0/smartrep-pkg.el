;;; -*- no-byte-compile: t -*-
(define-package "smartrep" "1.0.0" "Support sequential operation which omitted prefix keys." 'nil :commit "0b73bf3d1a3c795671bfee0a36cecfaa54729446" :keywords '("convenience") :authors '(("myuhe <yuhei.maeda_at_gmail.com>")) :maintainer '("myuhe") :url "https://github.com/myuhe/smartrep.el")
