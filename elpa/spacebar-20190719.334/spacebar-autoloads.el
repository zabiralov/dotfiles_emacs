;;; spacebar-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "spacebar" "spacebar.el" (0 0 0 0))
;;; Generated autoloads from spacebar.el

(defvar spacebar-mode nil "\
Non-nil if Spacebar mode is enabled.
See the `spacebar-mode' command
for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `spacebar-mode'.")

(custom-autoload 'spacebar-mode "spacebar" nil)

(autoload 'spacebar-mode "spacebar" "\
Toggle 'spacebar mode'.

  This global minor mode provides a tab-like bar for workspaces.

\(fn &optional ARG)" t nil)

(autoload 'spacebar-setup-evil-keys "spacebar" "\
Set up evil key bindings.

\(fn)" nil nil)

(provide 'spacebar)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "spacebar" '("spacebar-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; spacebar-autoloads.el ends here
