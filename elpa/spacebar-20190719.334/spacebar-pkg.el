;;; -*- no-byte-compile: t -*-
(define-package "spacebar" "20190719.334" "Workspaces Bar" '((eyebrowse "0.7.7") (emacs "25.4.0")) :commit "2b2cd0e786877273103f048e62a06b0027deca2d" :keywords '("convenience") :authors '(("Matthias Margush" . "matthias.margush@gmail.com")) :maintainer '("Matthias Margush" . "matthias.margush@gmail.com") :url "https://github.com/matthias-margush/spacebar")
