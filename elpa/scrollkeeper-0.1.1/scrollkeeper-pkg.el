;;; -*- no-byte-compile: t -*-
(define-package "scrollkeeper" "0.1.1" "Custom scrolling commands with visual guidelines" '((emacs "25.1")) :commit "3c4ac6b6b44686d31c260ee0b19daaee59bdccd6" :keywords '("convenience") :authors '(("Adam Porter" . "adam@alphapapa.net")) :maintainer '("Adam Porter" . "adam@alphapapa.net") :url "https://github.com/alphapapa/scrollkeeper.el")
