;;; cycle-quotes-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "cycle-quotes" "cycle-quotes.el" (0 0 0 0))
;;; Generated autoloads from cycle-quotes.el

(autoload 'cycle-quotes "cycle-quotes" "\
Cycle between string quote styles.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "cycle-quotes" '("cycle-quotes--")))

;;;***

;;;### (autoloads nil nil ("cycle-quotes-pkg.el" "cycle-quotes-test.el")
;;;;;;  (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; cycle-quotes-autoloads.el ends here
