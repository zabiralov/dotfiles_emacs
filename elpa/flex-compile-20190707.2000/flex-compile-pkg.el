(define-package "flex-compile" "20190707.2000" "Run, evaluate and compile for a many languages and modes."
  '((emacs "26")
    (dash "2.13.0")
    (buffer-manage "0.8"))
  :keywords
  '("compilation" "integration" "processes")
  :authors
  '(("Paul Landes"))
  :maintainer
  '("Paul Landes")
  :url "https://github.com/plandes/flex-compile")
;; Local Variables:
;; no-byte-compile: t
;; End:
