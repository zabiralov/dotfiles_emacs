;;; -*- no-byte-compile: t -*-
(define-package "dedicated" "1.0.0" "A very simple minor mode for dedicated buffers" 'nil :commit "8275fb672f9cc4ba6682ebda0ef91db827e32992" :keywords '("dedicated" "buffer") :authors '(("Eric Crampton" . "eric@atdesk.com")) :maintainer '("Eric Crampton" . "eric@atdesk.com"))
