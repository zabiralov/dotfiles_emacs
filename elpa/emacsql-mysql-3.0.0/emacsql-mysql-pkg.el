;;; -*- no-byte-compile: t -*-
(define-package "emacsql-mysql" "3.0.0" "EmacSQL back-end for MySQL" '((emacs "25.1") (emacsql "2.0.0")) :commit "8c5f095458aa37e4146b80d9319ee63571734127" :authors '(("Christopher Wellons" . "wellons@nullprogram.com")) :maintainer '("Christopher Wellons" . "wellons@nullprogram.com") :url "https://github.com/skeeto/emacsql")
