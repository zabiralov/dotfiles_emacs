;;; -*- no-byte-compile: t -*-
(define-package "vagrant" "0.6.1" "Manage a vagrant box from emacs" 'nil :commit "ef3022d290ee26597e21b17ab87acbd8d4f1071f" :keywords '("vagrant" "chef") :authors '(("Robert Crim" . "rob@servermilk.com")) :maintainer '("Robert Crim" . "rob@servermilk.com") :url "https://github.com/ottbot/vagrant.el")
