(define-package "choice-program" "0.9" "parameter based program"
  '((emacs "26")
    (cl-lib "1.0"))
  :keywords
  '("exec" "execution" "parameter" "option")
  :authors
  '(("Paul Landes"))
  :maintainer
  '("Paul Landes")
  :url "https://github.com/plandes/choice-program")
;; Local Variables:
;; no-byte-compile: t
;; End:
