;;; -*- no-byte-compile: t -*-
(define-package "resize-window" "0.7" "easily resize windows" '((emacs "24") (cl-lib "0.5")) :commit "dcbbd30f4f4435070a66a22c5a169b752ca9f904" :keywords '("window" "resize") :authors '(("Dan Sutton " . "danielsutton01@gmail.com")) :maintainer '("Dan Sutton " . "danielsutton01@gmail.com") :url "https://github.com/dpsutton/resize-mode")
