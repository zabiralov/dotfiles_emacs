;;; vc-fossil-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "vc-fossil" "vc-fossil.el" (0 0 0 0))
;;; Generated autoloads from vc-fossil.el

(autoload 'vc-fossil-registered "vc-fossil" "\
Check whether FILE is registered with fossil.

\(fn FILE)" nil nil)

(add-to-list 'vc-handled-backends 'Fossil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "vc-fossil" '("vc-fossil-")))

;;;***

;;;### (autoloads nil nil ("vc-fossil-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; vc-fossil-autoloads.el ends here
