(define-package "vala-snippets" "20150429.352" "Yasnippets for Vala"
  '((yasnippet "0.8.0"))
  :authors
  '(("Daniel Gopar"))
  :maintainer
  '("Daniel Gopar")
  :url "https://github.com/gopar/vala-snippets")
;; Local Variables:
;; no-byte-compile: t
;; End:
