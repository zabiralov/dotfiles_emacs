;;; -*- no-byte-compile: t -*-
(define-package "go-mode" "20190731.1751" "Major mode for the Go programming language" 'nil :commit "3ba198280bf8d5f2a4079618abfd3db2ebe04f58" :keywords '("languages" "go") :authors '(("The go-mode Authors")) :maintainer '("The go-mode Authors") :url "https://github.com/dominikh/go-mode.el")
