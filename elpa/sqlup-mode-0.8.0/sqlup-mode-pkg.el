;;; -*- no-byte-compile: t -*-
(define-package "sqlup-mode" "0.8.0" "Upcase SQL words for you" 'nil :commit "04970977b4abb4d44301651618bbf1cdb0b263dd" :keywords '("sql" "tools" "redis" "upcase") :authors '(("Aldric Giacomoni" . "trevoke@gmail.com")) :maintainer '("Aldric Giacomoni" . "trevoke@gmail.com") :url "https://github.com/trevoke/sqlup-mode.el")
