;;; -*- no-byte-compile: t -*-
(define-package "gitlab-ci-mode-flycheck" "20180304.1" "Flycheck support for ‘gitlab-ci-mode’" '((emacs "25") (flycheck "31") (gitlab-ci-mode "1")) :commit "388fd05f3ea88ed3ebafb09868fc021f6ecc7625" :keywords '("tools" "vc" "convenience") :authors '(("Joe Wreschnig")) :maintainer '("Joe Wreschnig") :url "https://gitlab.com/joewreschnig/gitlab-ci-mode-flycheck/")
