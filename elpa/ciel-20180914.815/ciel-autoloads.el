;;; ciel-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ciel" "ciel.el" (0 0 0 0))
;;; Generated autoloads from ciel.el

(autoload 'ciel-ci "ciel" "\
Clear Inside.

\(fn ARG)" t nil)

(autoload 'ciel-co "ciel" "\
COpy inside.

\(fn ARG)" t nil)

(autoload 'ciel-comment-region "ciel" "\


\(fn ARG)" t nil)

(autoload 'ciel-copy-to-register "ciel" "\


\(fn ARG REG)" t nil)

(autoload 'ciel-kill-region-paren "ciel" "\


\(fn ARG)" t nil)

(autoload 'ciel-copy-region-paren "ciel" "\


\(fn ARG)" nil nil)

(autoload 'ciel-kill-region-quote "ciel" "\


\(fn ARG)" nil nil)

(autoload 'ciel-copy-region-quote "ciel" "\


\(fn ARG)" nil nil)

(autoload 'ciel-kill-a-word "ciel" "\


\(fn)" nil nil)

(autoload 'ciel-copy-a-word "ciel" "\


\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ciel" '("ciel-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ciel-autoloads.el ends here
