;;; number-lock-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "number-lock" "number-lock.el" (0 0 0 0))
;;; Generated autoloads from number-lock.el

(autoload 'number-lock-toggle-number-lock "number-lock" "\
Toggle number-lock and most recent input method.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "number-lock" '("number-lock--last-input-method")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; number-lock-autoloads.el ends here
