;;; -*- no-byte-compile: t -*-
(define-package "number-lock" "1.0.0" "Enter symbols on your number keys without pressing shift" 'nil :commit "846e86e2b3b07410f69e70d3ba7afb072b5585da" :keywords '("convenience") :authors '(("Liu233w" . "wwwlsmcom@outlook.com")) :maintainer '("Liu233w" . "wwwlsmcom@outlook.com") :url "https://github.com/Liu233w/number-lock.el")
