;;; -*- no-byte-compile: t -*-
(define-package "vala-mode" "20150324.2225" "Vala mode derived mode" 'nil :commit "fb2871a4492d75d03d72e60474919ab89adb267b" :keywords '("vala" "languages" "oop") :authors '(("2005 Dylan R. E. Moonfire") ("       2008 Étienne BERSAC")) :maintainer '("Étienne BERSAC" . "bersace03@laposte.net"))
