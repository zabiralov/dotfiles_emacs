;;; -*- no-byte-compile: t -*-
(define-package "httprepl" "1.1" "An HTTP REPL" '((s "1.9.0") (dash "2.5.0") (emacs "24")) :commit "d2de8a676544deed1a5e084631a7799e487dbe55" :keywords '("http" "repl") :authors '(("Greg Sexton" . "gregsexton@gmail.com")) :maintainer '("Greg Sexton" . "gregsexton@gmail.com") :url "https://github.com/gregsexton/httprepl.el")
