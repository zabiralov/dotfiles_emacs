;;; init.el -- emacs main configuration file
;;;
;;; Author: Alexander E. Zabiralov

;;; Commentary:
;;;
;;; My GNU Emacs configuration

;; Byte-compiling Emacs config:
;; (byte-compile-file "init.el")'


;;; Code:

;; Manual-installed packages:
(let ((default-directory  "~/.emacs.d/lisp/"))
  (normal-top-level-add-subdirs-to-load-path))

;; Additional configuration:
(add-to-list 'load-path "~/.emacs.d/conf.d")

;; Recursive byte-compiling in lisp/:
;; (byte-recompile-directory (expand-file-name "~/.emacs.d/lisp") 0)


(setq custom-file "~/.emacs.d/conf.d/my-custom.el")
(load custom-file)

(require 'my-macros)
(require 'my-repos)
(require 'my-global)
(require 'my-buffers)
(require 'my-editing)
(require 'my-emacs)
(require 'my-progmodes)
(require 'my-markupmodes)
(require 'my-textmodes)
(require 'my-confmodes)
(require 'my-devopsmodes)
(require 'my-external)
(require 'my-eshell)
(require 'my-muse)
(require 'my-games)
(require 'my-windows)
(require 'my-minibuffer)
(require 'my-org)
(require 'my-telega)
(require 'my-dired)
(require 'my-screen)
(require 'my-faces)
(require 'my-functions)
(require 'my-keybindings)

;; set initial window layout
(add-hook 'after-init-hook 'my-emacs-startup)

;;; init.el ends here
