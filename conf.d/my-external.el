;;; external.el --- configuration for modes with external tools
;;;
;;; Time-stamp: <2019-08-02 17:56:43 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; slime :: emacs environment for Lisp programming
;; https://github.com/slime/slime
;; -----------------------------------------------

(require 'slime)
(setq inferior-lisp-program "/usr/bin/clisp")
(setq slime-contribs '(slime-fancy))


;; ac-slime :: autocompletion for slime
;; https://github.com/purcell/ac-slime
;; ------------------------------------

(require 'ac-slime)
(add-hook 'slime-mode-hook 'set-up-slime-ac)
(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
(add-to-list 'ac-modes 'slime-repl-mode)


;; httprepl :: make HTTP requests interactively
;; https://github.com/gregsexton/httprepl.el
;; --------------------------------------------

(require 'httprepl)
(setq httprepl-prompt "> ")

;; (defun my-httprepl ()
;;   "My function for start httprepl."
;;   (interactive)
;;   (make-frame-command)
;;   (fundamental-mode))
;;  ;(httprepl-mode))


;; gradle-mode :: integration with gradle build system
;; https://github.com/jacobono/emacs-gradle-mode
;; ---------------------------------------------------

(require 'gradle-mode)


;; google-this :: google text at emacs point
;; https://github.com/Malabarba/emacs-google-this
;; ----------------------------------------------

(require 'google-this)
(setq google-this-location-suffix "ru")
(setq google-this-wrap-in-quotes t)
(global-unset-key (kbd "C-x g"))
(global-set-key (kbd "C-x g") 'google-this-mode-submap)


;; sdcv-mode :: frontend to StartDict Console Version
;; https://github.com/pluskid/sdcv-mode/blob/master/sdcv-mode.el
;; -------------------------------------------------------------

(require 'sdcv)
(global-set-key (kbd "C-x t s") 'sdcv-search-input)
(global-set-key (kbd "C-x t t") 'sdcv-search-pointer)
(setq sdcv-word-pronounce nil)


;; magit :: advanced Git frontend
;; https://magit.vc/manual/magit/
;; ------------------------------

;; (require 'magit)
;; (global-set-key (kbd "C-x m") 'magit-status)


;; cliphist :: access to X11 clipboard manager
;; https://github.com/redguardtoo/cliphist
;; -------------------------------------------

(require 'cliphist)
(setq cliphist-linux-clipboard-managers '("parcellite"))


;; jq-format :: reformat JSON with jq(1)
;; https://github.com/wbolster/emacs-jq-format
;; -------------------------------------------

(require 'jq-format)
(setq jq-format-sort-keys t)


;; smart-compile :: smart compilation
;; https://github.com/zenitani/elisp/blob/master/smart-compile.el
;; --------------------------------------------------------------

(require 'smart-compile)
(setq smart-compile-make-program "make")

(add-to-list 'smart-compile-alist
             '("\\.go\\'" . "go build -v %f"))

(provide 'my-external)


;;; my-external.el ends here
