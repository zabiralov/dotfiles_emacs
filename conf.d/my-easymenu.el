;;; my-easymenu.el --- custom menus configuration
;;;
;;; Time-stamp: <2019-10-17 22:59:31 azabiralov>

;;; Commentary:

;;; Code:

(require 'easymenu)

(defvar my-custom-menu-map)

(easy-menu-define my-custom-menu global-map "MyCustom"
  '("Custom"
    ("This frame"
     ["Telega" telega t])
    ("New frame"
     ["Telega" my-telega])))




(provide 'my-easymenu)

;;; my-easymenu.el ends here
