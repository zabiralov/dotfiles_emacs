;;; buffers.el --- customization for Emacs buffers
;;;
;;; Time-stamp: <2019-06-25 17:29:16 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; dedicated :: mark selected buffer as dedicated
;; https://github.com/emacsorphanage/dedicated
;; ----------------------------------------------

(require 'dedicated)
(global-set-key (kbd "C-x d") 'dedicated-mode)


;; buffer-move :: move buffers
;; https://github.com/lukhas/buffer-move
;; -------------------------------------

(require 'buffer-move)
(global-set-key (kbd "<C-M-up>") 'buf-move-up)
(global-set-key (kbd "<C-M-down>") 'buf-move-down)
(global-set-key (kbd "<C-M-left>") 'buf-move-left)
(global-set-key (kbd "<C-M-right>") 'buf-move-right)


;; super-save :: automatic save for buffers
;; https://github.com/bbatsov/super-save
;; ----------------------------------------

(require 'super-save)
(setq super-save-auto-save-when-idle t)
(setq super-save-idle-duration 10)
(setq auto-save-default nil)
(super-save-mode t)


;; uniquify :: uniq buffer names
;; https://www.emacswiki.org/emacs/uniquify
;; http://pragmaticemacs.com/emacs/uniquify-your-buffer-names/
;; -----------------------------------------------------------

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
(setq uniquify-strip-common-suffix t)
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)
(setq uniquify-ignore-buffers-re "^\\*")


;; scratch-el :: per-buffer scratch buffers
;; https://github.com/ieure/scratch-el
;; ----------------------------------------

(require 'scratch)


;; ace-window :: switch between windows by number
;; https://github.com/abo-abo/ace-window
;; ----------------------------------------------

(require 'ace-window)
(setq aw-scope 'global)
(setq aw-keys '(?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9 ?0
                   ?q ?w ?e ?r ?t ?y ?u ?i ?o ?p
                   ?a ?s ?d ?f ?g ?h ?j ?k ?l
                   ?z ?x ?c ?v ?b ?n ?m))
(setq aw-leading-char-style 'char)
(setq aw-ignore-current t)


;; resize-window :: easy windows resize
;; https://github.com/dpsutton/resize-window
;; -----------------------------------------

(require 'resize-window)
(setq resize-window-coarse-argument 4)
(setq resize-window-fine-argument 1)


(provide 'my-buffers)

;;; my-buffers.el ends here
