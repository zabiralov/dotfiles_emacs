;;; my-telega.el --- Emacs Telegram client configuration
;;;
;;; Time-stamp: <2019-10-17 15:07:30 azabiralov>

;;; Commentary:

;;; Code:

(require 'telega)

(setq telega-language "ru"
      telega-debug nil
      telega-use-file-database t
      telega-use-message-database t
      telega-use-chat-info-database t
      telega-week-start-day 1
      telega-week-day-names '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat")
      telega-emoji-use-images nil
      telega-sticker-size '(3 . 24)
      telega-animation-play-inline nil
      telega-video-note-play-inline nil
      telega-root-buffer-name "*Telega*"
      telega-proxies '(:server "gilof.com"
                               :port 28443
                               :enable t
                               :type (:@type "proxyTypeMtproto" :secret 4be8a7958b6f0824cf524fcb425da8cf)))



(provide 'my-telega)

;;; my-telega.el ends here
