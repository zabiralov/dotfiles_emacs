;;; keybindings.el --- global Emacs keybindings rewrite
;;;
;;; Time-stamp: <2019-12-03 15:01:33 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; global hotkeys
;; --------------

(global-set-key [C-up] 'windmove-up)
(global-set-key [C-down] 'windmove-down)
(global-set-key [C-right] 'windmove-right)
(global-set-key [C-left] 'windmove-left)


(global-set-key [f1] 'my-switch-to-previous-buffer)
(global-set-key [S-f1] 'switch-to-buffer)
(global-set-key [C-f1] 'next-buffer)
(global-set-key [M-f1] 'previous-buffer)
;; (global-set-key [s-f1] ')
;; (global-set-key [H-f1] ')


(global-set-key [f2] 'save-buffer)
(global-set-key [S-f2] 'save-some-buffers)
(global-set-key [C-f2] 'my-rename-file-and-buffer)
;; (global-set-key [M-f2] ')
;; (global-set-key [s-f2] ')
;; (global-set-key [H-f2] ')


(global-set-key [f3] 'replace-string)
(global-set-key [S-f3] 'replace-regexp)
(global-set-key [C-f3] 'replace-rectangle)
(global-set-key [M-f3] 'sort-lines)
;; (global-set-key [s-f3] ')
;; (global-set-key [H-f3] ')


(global-set-key [f4] 'kmacro-start-macro)
(global-set-key [S-f4] 'kmacro-end-and-call-macro)
(global-set-key [C-f4] 'kmacro-call-macro)
;; (global-set-key [M-f4] ')
;; (global-set-key [s-f4] ')
;; (global-set-key [H-f4] ')


(global-set-key [f5] 'ace-window)
(global-set-key [S-f5] 'resize-window)
(global-set-key [C-f5] 'window-swap-states)
(global-set-key [M-f5] 'winner-undo)
(global-set-key [s-f5] 'winner-redo)
;; (global-set-key [H-f5] ')


(global-set-key [f6] 'yas-new-snippet)
(global-set-key [S-f6] 'yas-reload-all)
;; (global-set-key [C-f6] ')
;; (global-set-key [M-f6] ')
;; (global-set-key [s-f6] ')
;; (global-set-key [H-f6] ')


(global-set-key [f7] 'hs-toggle-hiding)
(global-set-key [S-f7] 'hs-hide-all)
(global-set-key [C-f7] 'hs-show-all)
;; (global-set-key [M-f7] ')
;;(global-set-key [s-f7] ')
;;(global-set-key [H-f7] ')


(global-set-key [f8] 'my-eshell-toggle)
;; (global-set-key [S-f8] 'my-term-toggle)
(global-set-key [C-f8] 'treemacs)
;; (global-set-key [M-f8] ')
(global-set-key [s-f8] 'my-fill-paragraph)
;; (global-set-key [H-f8] ')


(global-set-key [f9] 'highlight-symbol)
(global-set-key [S-f9] 'highlight-symbol-next)
(global-set-key [C-f9] 'highlight-symbol-query-replace)
;; (global-set-key [M-f9] ')
;; (global-set-key [s-f9] ')
;; (global-set-key [H-f9] ')


(global-set-key [f10] 'smart-compile)
;; (global-set-key [f10] ')
;; (global-set-key [S-f10] ')
;; (global-set-key [C-f10] ')
;; (global-set-key [M-f10] ')
;; (global-set-key [s-f10] ')
;; (global-set-key [H-f10] ')


(global-set-key [f11] 'toggle-frame-fullscreen)
(global-set-key [S-f11] 'menu-bar-mode)
(global-set-key [C-f11] 'new-frame)
;; (global-set-key [M-f11] ')
;; (global-set-key [s-f11] ')
;; (global-set-key [H-f11] ')

(global-set-key [f12] 'kill-buffer)
(global-set-key [S-f12] 'kill-buffer-and-window)
;; (global-set-key [C-f12] ')
;; (global-set-key [M-f12] ')
;; (global-set-key [s-f12] ')
;; (global-set-key [H-f12] ')


(global-set-key (kbd "s-<SPC>") 'my-switch-language)
(global-set-key (kbd "C-z") 'undo)

(global-set-key (kbd "C-x v") 'split-window-horizontally)
(global-set-key (kbd "C-x h") 'split-window-vertically)

(global-set-key [mouse-3] (popup-edit-menu-stub))
(global-set-key [mouse-8] 'previous-buffer)
(global-set-key [mouse-9] 'next-buffer)
(global-set-key [mouse-10] 'kill-this-buffer)
(global-set-key (kbd "<header-line> <mouse-2>") 'kill-this-buffer)
(global-set-key (kbd "<header-line> <mouse-3>") 'mouse-buffer-menu)

(global-set-key (kbd "<header-line> <mouse-4>") 'awesome-tab-backward-tab)
(global-set-key (kbd "<header-line> <mouse-5>") 'awesome-tab-forward-tab)

(global-set-key (kbd "<header-line> <S-mouse-4>") 'awesome-tab-backward-group)
(global-set-key (kbd "<header-line> <S-mouse-5>") 'awesome-tab-forward-group)

(global-set-key (kbd "<header-line> <C-mouse-4>") 'awesome-tab-move-current-tab-to-left)
(global-set-key (kbd "<header-line> <C-mouse-5>") 'awesome-tab-move-current-tab-to-right)

(global-set-key (kbd "<left-fringe> <mouse-4>") 'previous-error)
(global-set-key (kbd "<left-margin> <mouse-4>") 'previous-error)
(global-set-key (kbd "<left-fringe> <mouse-5>") 'next-error)
(global-set-key (kbd "<left-margin> <mouse-5>") 'next-error)

(global-set-key (kbd "<vertical-line> <mouse-4>") 'window-swap-states)
(global-set-key (kbd "<vertical-line> <mouse-5>") 'window-swap-states)


(global-set-key (kbd "C-x 1") 'delete-window)

(global-set-key (kbd "C-x t n") 'multi-term-next)
(global-set-key (kbd "C-x t p") 'multi-term-prev)

(global-set-key (kbd "<C-backspace>") 'my-backward-delete-line)
(global-set-key (kbd "<C-delete>") 'my-forward-delete-line)
(global-set-key (kbd "C-k") 'kill-line)

(global-set-key (kbd "<M-backspace>") 'my-backward-delete-word)
(global-set-key (kbd "<M-delete>") 'my-forward-delete-word)
(global-set-key (kbd "M-d") 'my-forward-delete-word)

(global-set-key (kbd "C-x C-n") 'deft-find-file)


(provide 'my-keybindings)


;;; my-keybindings.el ends here
