;;; my-games.el --- configuration for Emacs games
;;;
;;; Commentary:

;;; Code:


;; chess :: play chess in Emacs
;; https://github.com/jwiegley/emacs-chess
;; ---------------------------------------


(require 'chess)
(require 'chess-autosave)
(require 'chess-display)
(require 'chess-images)
(require 'chess-message)
(require 'chess-puzzle)

(setq chess-images-default-size 100)
(setq chess-autosave-file "~/.emacs.d/var/chess/autosave")
(setq chess-display-highlight-legal t)
(setq chess-display-highlight-last-move t)
(setq chess-images-separate-frame t)
(setq chess-message-language "russian")
(setq chess-puzzle-auto-next t)


(provide 'my-games)


;;; my-games.el ends here
