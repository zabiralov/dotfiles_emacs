;;; perfile.el --- customization for per-file modes
;;;
;;; Time-stamp: <2019-11-05 16:40:30 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; Load auto-complete for access to ac-modes var:
(require 'auto-complete)


;; logstash-conf-mode :: edit Logstash configuration file
;; https://github.com/Wilfred/logstash-conf.el
;; -------------------------------------------

(require 'logstash-conf)

(setq logstash-indent 2)


;; gitlab-ci-mode :: edit GitLab CI pipelines
;; https://github.com/emacsmirror/gitlab-ci-mode
;; ---------------------------------------------

(require 'gitlab-ci-mode)
(add-to-list 'auto-mode-alist '("\\.gitlab-ci.yml\\'" . gitlab-ci-mode))
(add-hook 'gitlab-ci-mode-hook 'my-default-modes)

(defvar gitlab-ci-tokens-file "~/.emacs.d/secrets/gitlab-tokens.el")
(load gitlab-ci-tokens-file)


;; text-mode :: edit plain text files
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Text-Mode.html
;; ------------------------------------------------------------------------

(require 'text-mode)

(dolist
    (my-text-ext '(("\\.txt\\'" . text-mode)
                   ("\\.text\\'" . text-mode)
                   ("\\.t\\'" . text-mode)))
  (add-to-list 'auto-mode-alist my-text-ext))


;; fluentd-mode :: edit Fluentd configuration files
;; https://github.com/syohex/emacs-fluentd-mode
;; ------------------------------------------------

(require 'fluentd-mode)
(add-hook 'fluentd-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'fluentd-mode)

(setq fluentd-ident-level 2)


;; toml-mode :: edit TOML files
;; https://github.com/dryman/toml-mode.el
;; --------------------------------------

(require 'toml-mode)
(add-to-list 'auto-mode-alist '("\\.toml\\'" . toml-mode))
(add-hook 'toml-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'toml-mode)


;; ssh-config-mode :: edit ~/.ssh/config
;; https://github.com/jhgorrell/ssh-config-mode-el
;; -----------------------------------------------

(require 'ssh-config-mode)
(add-hook 'ssh-config-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'ssh-config-mode)


;; rpm-spec-mode :: edit RPM spec files
;; https://github.com/stigbjorlykke/rpm-spec-mode
;; ----------------------------------------------

(require 'rpm-spec-mode)
(add-to-list 'auto-mode-alist '("\\.spec\\'" . rpm-spec-mode))
(add-hook 'sql-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'rpm-spec-mode)


;; sql-mode :: edit SQL sources
;; https://www.emacswiki.org/emacs/SqlMode
;; ---------------------------------------

(require 'sql)
(add-to-list 'auto-mode-alist '("\\.sql\\'" . sql-mode))
(add-hook 'sql-mode-hook 'my-default-modes)
(add-hook 'sql-mode-hook 'sqlup-mode)
(add-to-list 'ac-modes 'sql-mode)


;; markdown-mode :: edit Markdown files
;; https://jblevins.org/projects/markdown-mode/
;; --------------------------------------------

(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-hook 'markdown-mode-hook 'my-default-modes)
(add-hook 'markdown-mode-hook 'flyspell-mode)
(add-hook 'markdown-mode-hook 'guess-language-mode)
(add-to-list 'ac-modes 'markdown-mode)


;; lisp-mode :: edit Lisp sources
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/External-Lisp.html
;; ----------------------------------------------------------------------------

(require 'lisp-mode)
(add-to-list 'auto-mode-alist '("\\.lisp\\'" . lisp-mode))
(add-hook 'lisp-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'lisp-mode)


;; emacs-lisp-mode :: edit this sources
;; https://www.emacswiki.org/emacs/EmacsLispMode
;; ---------------------------------------------

(add-hook 'emacs-lisp-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'emacs-lisp-mode)

;; (local-set-key [f10] 'eval-last-sexp)


;; lua-mode :: edit Lua sources
;; http://immerrr.github.io/lua-mode/
;; ----------------------------------

(require 'lua-mode)
(add-to-list 'auto-mode-alist '("\\.lua\\'" . lua-mode))
(add-hook 'lua-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'lua-mode)


;; go-mode :: edit Golang sources
;; https://github.com/dominikh/go-mode.el
;; --------------------------------------

(require 'go-mode)
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
(add-hook 'go-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'go-mode)

(setq flycheck-go-vet-executable "go vet"
      flycheck-go-build-executable "go build")


;; json-mode :: edit JSON files
;; https://github.com/joshwnj/json-mode
;; ------------------------------------

(require 'json-mode)
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))
(add-hook 'json-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'json-mode)


;; sh-mode :: edit sh/bash shell scripts
;; https://www.emacswiki.org/emacs/ShMode
;; --------------------------------------

(require 'shell)

(dolist
    (my-sh-ext '(("\\.bash\\'" . sh-mode)
                 ("\\.sh\\'" . sh-mode)
                 ("\\.csh\\'" . sh-mode)
                 ("\\.tcsh\\'" . sh-mode)
                 ("\\.zsh\\'" . sh-mode)
                 ("\\.ksh\\'" . sh-mode)
                 ("\\.ash\\'" . sh-mode)))
  (add-to-list 'auto-mode-alist my-sh-ext))

(add-hook 'sh-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'sh-mode)


;; conf-mode :: edit various configuration files
;; http://doc.endlessparentheses.com/Fun/conf-mode.html
;; ----------------------------------------------------

(require 'conf-mode)

(dolist
    (my-conf-ext '(("\\.conf\\'" . conf-mode)
                   ("\\.cnf\\'" . conf-mode)
                   ("\\.cf\\'" . conf-mode)
                   ("\\.config\\'" . conf-mode)))
  (add-to-list 'auto-mode-alist my-conf-ext))

(add-hook 'conf-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'conf-mode)


;; cperl-mode :: advanced mode for edit Perl5 sources
;; https://www.emacswiki.org/emacs/CPerlMode
;; --------------------------------------------------

(require 'cperl-mode)
(defalias 'perl-mode 'cperl-mode)
(add-to-list 'ac-modes 'perl-mode)
(add-hook 'perl-mode-hook 'my-default-modes)
(setq cperl-hairy nil
      cperl-indent-level 4
      cperl-auto-newline t
      cperl-electric-parens nil
      cperl-electric-lbrace-space nil)


;; dns-mode :: emacs mode for edit BIND DNS zones
;; https://github.com/emacs-mirror/emacs/blob/master/lisp/textmodes/dns-mode.el
;; ----------------------------------------------------------------------------

(require 'dns-mode)

(dolist
    (my-dns-ext '(("\\.dns\\'" . dns-mode)
                  ("\\.zone\\'" . dns-mode)
                  ("\\.bind\\'" . dns-mode)))
  (add-to-list 'auto-mode-alist my-dns-ext))

(add-hook 'dns-mode-hook 'my-default-modes)
(setq dns-mode-soa-auto-increment-serial t)


;; yaml-mode :: edit YAML files
;; https://github.com/yoshiki/yaml-mode
;; ------------------------------------

(require 'yaml-mode)

(dolist
    (my-yaml-ext '(("\\.yml\\'" . yaml-mode)
                   ("\\.yaml\\'" . yaml-mode)))
  (add-to-list 'auto-mode-alist my-yaml-ext))

(add-to-list 'ac-modes 'yaml-mode)
(add-hook 'yaml-mode-hook 'my-default-modes)


;; ansible :: edit Ansible playbooks
;; https://github.com/k1LoW/emacs-ansible
;; --------------------------------------

(require 'ansible)
(require 'ansible-doc)
(add-hook 'yaml-mode-hook '(lambda () (ansible t)))
(add-hook 'yaml-mode-hook '(lambda () (ansible-doc-mode t)))


;; jinja2-mode :: edit Jinja2 templates
;; https://github.com/paradoxxxzero/jinja2-mode
;; --------------------------------------------

(require 'jinja2-mode)

(dolist
    (my-jinja-ext '(("\\.j2\\'" . jinja2-mode)
                    ("\\.jinja\\'" . jinja2-mode)
                    ("\\.jinja2\\'" . jinja2-mode)))
  (add-to-list 'auto-mode-alist my-jinja-ext))

(add-to-list 'ac-modes 'jinja2-mode)
(add-hook 'jinja2-mode-hook 'my-default-modes)


;; vala-mode :: edit Vala sources
;; https://github.com/emacsorphanage/vala-mode
;; -------------------------------------------

(require 'vala-mode)
(add-to-list 'auto-mode-alist '("\\.vala\\'" . vala-mode))
(add-to-list 'ac-modes 'vala-mode)
(add-hook 'vala-mode-hook 'my-default-modes)


;; dockerfile-mode :: edit Dockerfiles
;; https://github.com/spotify/dockerfile-mode
;; ------------------------------------------

(require 'dockerfile-mode)
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))
(add-to-list 'dockerfile-mode-hook 'my-default-modes)


;; docker-compose-mode :: edit docker-compose manifests
;; https://github.com/meqif/docker-compose-mode
;; ----------------------------------------------------

(require 'docker-compose-mode)
(add-to-list 'auto-mode-alist '("docker\\-compose\\.yml\\'" . docker-compose-mode))
(add-to-list 'docker-compose-mode-hook 'my-default-modes)


;; cc-mode :: edit souces for c-style languages
;; https://www.gnu.org/software/emacs/manual/html_mono/ccmode.html
;; ---------------------------------------------------------------

(require 'cc-mode)
(setq c-basic-offset 4
      c-default-style "stroustrup"
      c-report-syntactic-errors t)


(add-hook 'c-mode-hook 'my-default-modes)

(add-hook 'c-mode-hook (lambda ()
                         (setq flycheck-gcc-include-path (list (expand-file-name "~/include/") (expand-file-name "/usr/include/"))
                               flycheck-clang-include-path (list (expand-file-name "~/include/") (expand-file-name "/usr/include/"))
                               flycheck-relevant-error-other-file-show nil)
                         (highlight-symbol-mode -1)
                         (highlight-symbol-nav-mode -1)
                         (aggressive-indent-mode -1)
                         (c-toggle-comment-style -1)
                         (c-toggle-electric-state -1)
                         (c-toggle-hungry-state -1)
                         (c-toggle-auto-newline)
                         (c-toggle-syntactic-indentation)
                         (subword-mode)))

(add-to-list 'ac-modes 'c-mode)

(setq flycheck-gcc-language-standard "c11")

(add-to-list 'flycheck-gcc-include-path '/home/azabiralov/include/slre-1.3)


;; apache-mode :: edit Apache HTTPD server configs
;; https://github.com/emacs-php/apache-mode
;; -----------------------------------------------

;; !! Obsolete package - removed on 25/09/19

;; (require 'apache-mode)
;; (add-hook 'apache-mode-hook 'my-default-modes)


;; sed-mode :: edit sed scripts
;; https://elpa.gnu.org/packages/sed-mode.html
;; -------------------------------------------

(require 'sed-mode)
(add-to-list 'auto-mode-alist '("\\.sed\\'" . sed-mode))
(add-hook 'sed-mode-hook 'my-default-modes)


;; cql-mode :: edit Cassandra CQL sources
;; https://github.com/Yuki-Inoue/cql-mode
;; --------------------------------------

(require 'cql-mode)
(add-to-list 'auto-mode-alist '("\\.cql\\'" . cql-mode))
(add-hook 'cql-mode-hook 'my-default-modes)


;; nginx-mode :: edit Nginx web server configs
;; https://github.com/ajc/nginx-mode
;; -------------------------------------------

(require 'nginx-mode)
(add-hook 'nginx-mode-hook 'my-default-modes)


;; junos-mode :: edit JunOS configuration files
;; https://github.com/vincentbernat/junos-mode
;; --------------------------------------------

(require 'junos-mode)

(dolist
    (my-juniper-ext '(("\\.jcfg\\'" . junos-mode)
                      ("\\.juniper\\'" . junos-mode)
                      ("\\.junos\\'" . junos-mode)
                      ("\\.jun\\'" . junos-mode)))
  (add-to-list 'auto-mode-alist my-juniper-ext))

(add-hook 'junos-mode-hook 'my-default-modes)


;; latex-mode :: edit LaTeX documents
;; https://www.emacswiki.org/emacs/LaTeX
;; -------------------------------------

(require 'reftex)
(setq latex-run-command "pdflatex")
(add-hook 'latex-mode-hook 'turn-on-reftex)
(add-hook 'latex-mode-hook 'my-default-modes)
(local-set-key [f10] 'tex-file)


;; systemd-mode :: edit systemd unit files
;; https://github.com/holomorph/systemd-mode
;; -----------------------------------------

(require 'systemd)
(add-to-list 'auto-mode-alist '("\\.service\\'" . systemd-mode))
(add-hook 'systemd-mode-hook 'my-default-modes)


;; groovy-mode :: edit Groovy and Gradle sources
;; https://github.com/Groovy-Emacs-Modes/groovy-emacs-modes
;; --------------------------------------------------------

(require 'groovy-mode)

(dolist
    (my-groovy-ext '(("\\.gradle\\'" . groovy-mode)
                     ("\\.groovy\\'" . groovy-mode)
                     ("\\.gvy\\'" . groovy-mode)
                     ("\\.gy\\'" . groovy-mode)
                     ("\\.gsh\\'" . groovy-mode)))
  (add-to-list 'auto-mode-alist my-groovy-ext))

(add-hook 'groovy-mode-hook 'my-default-modes)

(setq groovy-indent-offset 2)
(setq groovy-highlight-assignments t)


;; rfc-mode :: read RFC documents
;; https://github.com/galdor/rfc-mode
;; ----------------------------------

(require 'rfc-mode)
(setq rfc-mode-directory (expand-file-name "~/doc/rfc/"))


(provide 'my-perfile)


;;; my-perfile.el ends here
