;;; global.el --- global Emacs configuration
;;;
;;; Time-stamp: <2019-10-08 13:02:33 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; overwrite environment variables
;; -------------------------------

(setenv "MANWIDTH" "80")

;; global Emacs variables
;; ----------------------

(setq inhibit-splash-screen t
      make-backup-files nil
      auto-mode-alist (append '(("/tmp/mutt.*" . mail-mode)) auto-mode-alist)
      max-mini-window-height 0.5
      x-stretch-cursor t)


(dolist
    (my-init-frame-options '((fullscreen . maximized)))
  (add-to-list 'initial-frame-alist my-init-frame-options))


(dolist
    (my-def-frame-options '((fullscreen . maximized)))
  (add-to-list 'default-frame-alist my-def-frame-options))


;; enable/disable basic minor modes on startup
;; -------------------------------------------

;; disabled modes:
(column-number-mode 0)
(desktop-save-mode 0)
(line-number-mode 0)
(scroll-bar-mode 0)
(tool-bar-mode 0)

;; enabled modes:
(blink-cursor-mode t)
(delete-selection-mode t)
(global-hl-line-mode t)
(menu-bar-mode t)
(winner-mode t)

(windmove-default-keybindings)

;; default new buffer settings
;; ---------------------------

(setq-default abbrev-mode t
              auto-insert-mode t
              cursor-type 'bar
              tab-width 2
              fill-column 80
              major-mode 'conf-mode
              indent-tabs-mode nil
              mode-line-format
              (list "%+ " "%4l %4c " "%6p " "%b " "%f " "%e" mode-line-modes))


;; human languages support
;; -----------------------

(set-language-environment 'utf-8)
(setq default-input-method 'russian-computer)

;; color themes
;; ------------

;; (add-to-list 'load-path "~/.emacs.d/themes/solarized")
;; (add-to-list 'custom-theme-load-path "~/.emacs.d/themes/solarized")

(load-theme 'solarized-dark t)


;; kill-ring :: kill ring settings
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Kill-Ring.html
;; ------------------------------------------------------------------------

(setq kill-ring-max 100)



(provide 'my-global)

;;; my-global.el ends here
