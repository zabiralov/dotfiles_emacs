;;; my-textmodes.el --- customizations for text modes
;;;
;;; Time-stamp: <2019-11-07 14:47:18 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; Load auto-complete for access to ac-modes var:
(require 'auto-complete)


;; text-mode :: edit plain text files
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Text-Mode.html
;; ------------------------------------------------------------------------

(require 'text-mode)

(dolist
    (my-text-ext '(("\\.txt\\'" . text-mode)
                   ("\\.text\\'" . text-mode)
                   ("\\.t\\'" . text-mode)))
  (add-to-list 'auto-mode-alist my-text-ext))


;; latex-mode :: edit LaTeX documents
;; https://www.emacswiki.org/emacs/LaTeX
;; -------------------------------------

(require 'reftex)
(setq latex-run-command "pdflatex")
(add-hook 'latex-mode-hook 'turn-on-reftex)
(add-hook 'latex-mode-hook 'my-default-modes)
(local-set-key [f10] 'tex-file)


;; rfc-mode :: read RFC documents
;; https://github.com/galdor/rfc-mode
;; ----------------------------------

(require 'rfc-mode)
(setq rfc-mode-directory (expand-file-name "~/doc/rfc/"))


(provide 'my-textmodes)

;;; my-textmodes.el ends here
