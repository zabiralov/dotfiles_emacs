;;; my-minibuffer.el --- minibuffer settings

;; Time-stamp: <2019-07-09 13:04:40 azabiralov>

;;; Commentary:

;;; Code:

(add-hook 'minibuffer-setup-hook
          '(lambda()
             (highlight-symbol-mode -1)))


(provide 'my-minibuffer)


;;; my-minibuffer.el ends here
