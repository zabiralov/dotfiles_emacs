;;; my-markupmodes.el --- customizations for markup modes
;;;
;;; Time-stamp: <2019-11-07 14:45:06 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; Load auto-complete for access to ac-modes var:
(require 'auto-complete)


;; gitlab-ci-mode :: edit GitLab CI pipelines
;; https://github.com/emacsmirror/gitlab-ci-mode
;; ---------------------------------------------

(require 'gitlab-ci-mode)
(add-to-list 'auto-mode-alist '("\\.gitlab-ci.yml\\'" . gitlab-ci-mode))
(add-hook 'gitlab-ci-mode-hook 'my-default-modes)

(defvar gitlab-ci-tokens-file "~/.emacs.d/secrets/gitlab-tokens.el")
(load gitlab-ci-tokens-file)


;; toml-mode :: edit TOML files
;; https://github.com/dryman/toml-mode.el
;; --------------------------------------

(require 'toml-mode)
(add-to-list 'auto-mode-alist '("\\.toml\\'" . toml-mode))
(add-hook 'toml-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'toml-mode)


;; markdown-mode :: edit Markdown files
;; https://jblevins.org/projects/markdown-mode/
;; --------------------------------------------

(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-hook 'markdown-mode-hook 'my-default-modes)
(add-hook 'markdown-mode-hook 'flyspell-mode)
(add-hook 'markdown-mode-hook 'guess-language-mode)
(add-to-list 'ac-modes 'markdown-mode)


;; json-mode :: edit JSON files
;; https://github.com/joshwnj/json-mode
;; ------------------------------------

(require 'json-mode)
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))
(add-hook 'json-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'json-mode)


;; yaml-mode :: edit YAML files
;; https://github.com/yoshiki/yaml-mode
;; ------------------------------------

(require 'yaml-mode)

(dolist
    (my-yaml-ext '(("\\.yml\\'" . yaml-mode)
                   ("\\.yaml\\'" . yaml-mode)))
  (add-to-list 'auto-mode-alist my-yaml-ext))

(add-to-list 'ac-modes 'yaml-mode)
(add-hook 'yaml-mode-hook 'my-default-modes)


;; jinja2-mode :: edit Jinja2 templates
;; https://github.com/paradoxxxzero/jinja2-mode
;; --------------------------------------------

(require 'jinja2-mode)

(dolist
    (my-jinja-ext '(("\\.j2\\'" . jinja2-mode)
                    ("\\.jinja\\'" . jinja2-mode)
                    ("\\.jinja2\\'" . jinja2-mode)))
  (add-to-list 'auto-mode-alist my-jinja-ext))

(add-to-list 'ac-modes 'jinja2-mode)
(add-hook 'jinja2-mode-hook 'my-default-modes)


(provide 'my-markupmodes)

;;; my-markupmodes.el ends here
