;;; my-org.el --- Emacs deft/org modes configuration
;;;
;;; Time-stamp: <2019-07-11 13:24:47 azabiralov>

;;; Commentary:

;;; Code:


;; deft :: Emacs plain-text notes organizer
;; https://jblevins.org/projects/deft/
;; ----------------------------------------

(require 'deft)

(setq deft-extensions '("txt" "org"))
(setq deft-directory "~/Notes")
(setq deft-recursive t)




;; Org-mode :: Emacs organizer
;;
;;


(require 'org)

(set-face-attribute 'org-document-title nil
                    :weight 'bold
                    :underline t
                    :font "Noto Sans Mono-17")

(set-face-attribute 'org-level-1 nil
                    :weight 'bold
                    :underline t
                    :font "Noto Sans Mono-17")

(set-face-attribute 'org-level-2 nil
                    :weight 'bold
                    :font "Noto Sans Mono-17")

(set-face-attribute 'org-level-3 nil
                    :weight 'bold
                    :underline t
                    :font "Noto Sans Mono-16")

(set-face-attribute 'org-level-4 nil
                    :weight 'bold
                    :font "Noto Sans Mono-16")

(set-face-attribute 'org-level-5 nil
                    :weight 'bold
                    :underline t
                    :font "Noto Sans Mono-15")

(set-face-attribute 'org-level-6 nil
                    :weight 'bold
                    :font "Noto Sans Mono-15")

(set-face-attribute 'org-level-7 nil
                    :weight 'bold
                    :underline t
                    :font "Noto Sans Mono-14")

(set-face-attribute 'org-level-8 nil
                    :weight 'bold
                    :font "Noto Sans Mono-14")




(provide 'my-org)

;;; my-org.el ends here
