;;; my-windows.el --- Emacs windows configuration and modes
;;;
;;; Time-stamp: <2019-07-12 15:13:12 azabiralov>

;;; Commentary:

;;; Code:


;; emacs-puppose :: emacs window layout manager
;; https://github.com/bmag/emacs-purpose
;; --------------------------------------------

(require 'window-purpose)


(setq purpose-default-layout-file "~/.emacs.d/layouts/default.window-layout")
(setq purpose-display-at-top-height 8)
(setq purpose-display-at-bottom-height 8)
(setq purpose-display-at-left-width 32)
(setq purpose-display-at-right-width 32)
(setq purpose-use-default-configuration t)

;; (purpose-mode t)


;; popwin :: Emacs pop-up windows
;; https://github.com/m2ym/popwin-el
;; ---------------------------------

(require 'popwin)

(setq popwin:popup-window-position 'top)
(setq popwin:popup-window-width 30)
(setq popwin:popup-window-height 20)
(setq popwin:reuse-window 'current)
(setq popwin:adjust-other-windows t)
(setq popwin:special-display-config
      '(
        ("*Backtrace*" :stick t)
        ("*my-eshell*" :stick t :position bottom :height 20)
        ("*my-term*" :stick t :position bottom :height 20)
        "*Shell Command Output*"
        "*slime-apropos*"
        "*slime-description*"
        "*slime-macroexpansion*"
        "*slime-xref*"
        "*vc-change-log*"
        "*vc-diff*"
        (" *undo-tree*" :width 60 :position right)
        ("*Miniedit Help*" :noselect t)
        ("*Pp Macroexpand Output*" :noselect t)
        ("*slime-compilation*" :noselect t)
        ("^\\*anything.*\\*$" :regexp t)
        (compilation-mode :noselect t)
        (completion-list-mode :noselect t)
        (grep-mode :noselect t)
        (occur-mode :noselect t)
        (sldb-mode :stick t)
        (help-mode :stick t)
        (helpful-mode :stick t)
        slime-connection-list-mode
        slime-repl-mode
        ))


(popwin-mode t)




(provide 'my-windows)

;;; my-windows.el ends here
