;;; repos.el --- Emacs packages sources configration
;;;
;;; Time-stamp: <2019-10-08 02:30:51 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; package management
;; ------------------

(require 'package)
(package-initialize)
(setq package-archives
      '(("gnu"     . "http://elpa.gnu.org/packages/")
        ("stable"  . "http://stable.melpa.org/packages/")
        ("current" . "http://melpa.org/packages/")))

(setq package-archive-priorities
      '(("stable"  . 3)
        ("gnu"     . 2)
        ("current" . 1)
        ("sunrise" . 1)))

(setq package-check-signature nil)

(provide 'my-repos)

;;; my-repos.el ends here
