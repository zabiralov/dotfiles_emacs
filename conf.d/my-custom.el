;;; my-custom.el --- file for saving 'custom' settings

;;; Commentary:

;;; Configuration options added by custom

;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (zones phi-search mc-extras multiple-cursors muse opencl-mode number-lock newlisp-mode lxd-tramp kill-ring-search k8s-mode json-navigator insert-shebang ipcalc ini-mode go-autocomplete dired-subtree dired-open dired-single centered-cursor-mode d-mode cql-mode comment-or-uncomment-sexp ciel basic-c-compile logstash-conf es-mode robots-txt-mode apache-mode popup-edit-menu telega restclient solarized-theme smart-mode-line flycheck-pos-tip column-enforce-mode open-junk-file indent-guide move-text flex-compile elpl dashboard spacebar validate guess-language spaceline treemacs-projectile treemacs-magit treemacs-icons-dired treemacs zygospore workgroups2 all-the-icons-dired all-the-icons vc-fossil gist emacsql-sqlite emacsql-psql emacsql-mysql emacsql jedi elpy smart-compile flycheck-golangci-lint kubernetes fill-column-indicator fullframe popwin ob-sql-mode deft highlight-symbol window-purpose geiser chess sdcv go-eldoc c-eldoc diminish rfc-mode region-state scrollkeeper go-snippets vala-snippets elmacro clojure-snippets browse-kill-ring resize-window gitlab-ci-mode-flycheck gitlab-ci-mode groovy-mode gradle-mode flycheck-gradle jq-format ssh-config-mode httprepl gorepl-mode yatemplate helpful rainbow-delimiters back-button ac-slime diffview sqlup-mode speed-type mines sokoban google-this google-translate docker-compose-mode dedicated common-lisp-snippets ansible-doc cycle-quotes csv-mode terraform-mode vagrant yasnippet-snippets yasnippet-classic-snippets powershell buffer-move scratch restart-emacs ws-butler iedit aggressive-indent ace-window smartparens sed-mode super-save volatile-highlights perspective systemd windresize multi-term git-gutter yasnippet yaml-mode vala-mode toml-mode slime showtip rpm-spec-mode rich-minority pos-tip persp-mode nyan-mode nginx-mode markdown-mode magit lua-mode jinja2-mode go-mode flycheck-yamllint dockerfile-mode docker common-header-mode-line cmake-mode cliphist cider auto-complete ansible))))



(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide 'my-custom)

;;; my-custom.el ends here
