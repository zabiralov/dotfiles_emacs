;;; my-progmodes.el --- customization for progmodes
;;;
;;; Time-stamp: <2019-11-14 23:04:05 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; Load auto-complete for access to ac-modes var:
(require 'auto-complete)
(require 'flycheck)


;; sql-mode :: edit SQL sources
;; https://www.emacswiki.org/emacs/SqlMode
;; ---------------------------------------

(require 'sql)
(add-to-list 'auto-mode-alist '("\\.sql\\'" . sql-mode))
(add-hook 'sql-mode-hook 'my-default-modes)
(add-hook 'sql-mode-hook 'sqlup-mode)
(add-to-list 'ac-modes 'sql-mode)


;; lisp-mode :: edit Lisp sources
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/External-Lisp.html
;; ----------------------------------------------------------------------------

(require 'lisp-mode)
(add-to-list 'auto-mode-alist '("\\.lisp\\'" . lisp-mode))
(add-hook 'lisp-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'lisp-mode)


;; emacs-lisp-mode :: edit this sources
;; https://www.emacswiki.org/emacs/EmacsLispMode
;; ---------------------------------------------

(add-hook 'emacs-lisp-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'emacs-lisp-mode)

;; (local-set-key [f10] 'eval-last-sexp)


;; lua-mode :: edit Lua sources
;; http://immerrr.github.io/lua-mode/
;; ----------------------------------

(require 'lua-mode)
(add-to-list 'auto-mode-alist '("\\.lua\\'" . lua-mode))
(add-hook 'lua-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'lua-mode)


;; go-mode :: edit Golang sources
;; https://github.com/dominikh/go-mode.el
;; --------------------------------------

(require 'go-mode)
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
(add-hook 'go-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'go-mode)

(setq flycheck-go-vet-executable "go vet"
      flycheck-go-build-executable "go build")




;; sh-mode :: edit sh/bash shell scripts
;; https://www.emacswiki.org/emacs/ShMode
;; --------------------------------------

(require 'shell)

(dolist
    (my-sh-ext '(("\\.bash\\'" . sh-mode)
                 ("\\.sh\\'" . sh-mode)
                 ("\\.csh\\'" . sh-mode)
                 ("\\.tcsh\\'" . sh-mode)
                 ("\\.zsh\\'" . sh-mode)
                 ("\\.ksh\\'" . sh-mode)
                 ("\\.ash\\'" . sh-mode)))
  (add-to-list 'auto-mode-alist my-sh-ext))

(add-hook 'sh-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'sh-mode)


;; cperl-mode :: advanced mode for edit Perl5 sources
;; https://www.emacswiki.org/emacs/CPerlMode
;; --------------------------------------------------

(require 'cperl-mode)
;; (defalias 'perl-mode 'cperl-mode)
(add-to-list 'ac-modes 'perl-mode)
(add-hook 'perl-mode-hook 'my-default-modes)
(setq cperl-hairy nil
      cperl-indent-level 4
      cperl-auto-newline t
      cperl-electric-parens nil
      cperl-electric-lbrace-space nil)


;; vala-mode :: edit Vala sources
;; https://github.com/emacsorphanage/vala-mode
;; -------------------------------------------

(require 'vala-mode)
(add-to-list 'auto-mode-alist '("\\.vala\\'" . vala-mode))
(add-to-list 'ac-modes 'vala-mode)
(add-hook 'vala-mode-hook 'my-default-modes)


;; cc-mode :: edit souces for c-style languages
;; https://www.gnu.org/software/emacs/manual/html_mono/ccmode.html
;; ---------------------------------------------------------------

(require 'cc-mode)
(setq c-basic-offset 4
      c-default-style "stroustrup"
      c-report-syntactic-errors t)


(add-hook 'c-mode-hook 'my-default-modes)

(add-hook 'c-mode-hook (lambda ()
                         (setq flycheck-gcc-include-path (list (expand-file-name "~/include/") (expand-file-name "/usr/include/"))
                               flycheck-clang-include-path (list (expand-file-name "~/include/") (expand-file-name "/usr/include/"))
                               flycheck-relevant-error-other-file-show nil)
                         (highlight-symbol-mode -1)
                         (highlight-symbol-nav-mode -1)
                         (aggressive-indent-mode -1)
                         (c-toggle-comment-style -1)
                         (c-toggle-electric-state -1)
                         (c-toggle-hungry-state -1)
                         (c-toggle-auto-newline)
                         (c-toggle-syntactic-indentation)
                         (subword-mode)))

(add-to-list 'ac-modes 'c-mode)

(setq flycheck-gcc-language-standard "c11")

(add-to-list 'flycheck-gcc-include-path '/home/azabiralov/include/slre-1.3)


;; sed-mode :: edit sed scripts
;; https://elpa.gnu.org/packages/sed-mode.html
;; -------------------------------------------

(require 'sed-mode)
(add-to-list 'auto-mode-alist '("\\.sed\\'" . sed-mode))
(add-hook 'sed-mode-hook 'my-default-modes)


;; cql-mode :: edit Cassandra CQL sources
;; https://github.com/Yuki-Inoue/cql-mode
;; --------------------------------------

(require 'cql-mode)
(add-to-list 'auto-mode-alist '("\\.cql\\'" . cql-mode))
(add-hook 'cql-mode-hook 'my-default-modes)


;; groovy-mode :: edit Groovy and Gradle sources
;; https://github.com/Groovy-Emacs-Modes/groovy-emacs-modes
;; --------------------------------------------------------

(require 'groovy-mode)

(dolist
    (my-groovy-ext '(("\\.gradle\\'" . groovy-mode)
                     ("\\.groovy\\'" . groovy-mode)
                     ("\\.gvy\\'" . groovy-mode)
                     ("\\.gy\\'" . groovy-mode)
                     ("\\.gsh\\'" . groovy-mode)))
  (add-to-list 'auto-mode-alist my-groovy-ext))

(add-hook 'groovy-mode-hook 'my-default-modes)

(setq groovy-indent-offset 2)
(setq groovy-highlight-assignments t)


(provide 'my-progmodes)

;;; my-progmodes.el ends here
