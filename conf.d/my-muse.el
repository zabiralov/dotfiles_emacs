;;; my-muse.el --- Emacs Muse configuration
;;;
;;; Time-stamp: <2019-11-29 15:37:09 azabiralov>

;;; Commentary:

;;; Code:

(require 'muse)

(require 'muse-mode)
(require 'muse-html)
(require 'muse-latex)
(require 'muse-texinfo)
(require 'muse-docbook)
(require 'muse-book)



(provide 'my-muse)

;;; my-muse.el ends here
