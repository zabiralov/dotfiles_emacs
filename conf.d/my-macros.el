;;; my-macros.el --- List of saved keyboard macroses
;;;
;;; Time-stamp: <2019-11-08 14:08:51 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; keyboard macros :: https://www.emacswiki.org/emacs/KeyboardMacros
;; -----------------------------------------------------------------

;; Вставка комментария и горизонтальной линии из '-' длиной 77 символов:
(fset 'macro-insert-commented-line
      [?\M-\; ?\C-u ?7 ?7 ?- return])

(global-set-key (kbd "S-<f9>") 'macro-insert-commented-line)

;; ---

;; Переход на новую строку и вставка комментария:
(fset 'macro-return-then-insert-comment
   "\C-m\C-[;")

;; ---

;; Выравнивание параграфа по обоим сторонам (по ширине):
(fset 'macro-fill-by-width
   "\C-u\C-[q")

;; ---

;; Переключиться на другое окно:
(fset 'macro-switch-to-other-window
   "\C-xo")

;; ---

;; Тab + Down
(fset 'macro-tab-and-down
      "\C-i\C-[OB")


;; ---

;; Удаление содержимого всей строки
;; (для работы должен быть включен delete-selection-mode):
;; C-a, C-SPC, C-e, DEL
(fset 'macro-purge-line
      "\C-a\C-@\C-e\C-[[3~\C-[xend\C-ik\C-i")

(provide 'my-macros)

;;; my-macros.el ends here
