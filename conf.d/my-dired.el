;;; my-dired.el --- configuration for Dired and related modes
;;;
;;; Commentary:

;;; Code:

(require 'dired)
(require 'dired-x)
(require 'dired-aux)
(require 'dired-single)
(require 'dired-open)
(require 'dired-subtree)

(setq dired-backup-overwrite nil
      dired-isearch-filenames nil
      dired-subdir-switches nil
      dired-use-ls-dired t
      dired-ls-F-marks-symlinks t
      dired-keep-marker-rename t
      dired-dwim-target t
      dired-copy-preserve-time t
      dired-load-hook nil
      dired-mode-hook nil
      dired-before-readin-hook nil
      dired-after-readin-hook nil
      dired-initial-position-hook nil
      dired-hide-details-hide-symlink-targets t
      dired-hide-details-hide-information-lines t
      dired-always-read-filesystem nil
      dired-auto-revert-buffer nil
      dired-recursive-deletes 'top
      dired-no-confirm nil
      dired-recursive-copies 'top
      dired-bind-vm nil
      dired-bind-jump t
      dired-bind-man t
      dired-bind-info t
      dired-vm-read-only-folders nil
      dired-omit-size-limit 30000
      dired-omit-case-fold 'filesystem
      dired-omit-verbose t
      dired-find-subdir nil
      dired-guess-shell-gnutar t
      dired-guess-shell-gzip-quiet t
      dired-guess-shell-znew-switches nil
      dired-clean-up-buffers-too t
      dired-clean-confirm-killing-deleted-buffers t
      dired-guess-shell-alist-user nil
      dired-guess-shell-case-fold-search t
      dired-x-hands-off-my-keys t
      dired-single-use-magic-buffer nil
      dired-subtree-line-prefix "   "
      dired-subtree-cycle-depth 5)


(provide 'my-dired)


;;; my-dired.el ends here
