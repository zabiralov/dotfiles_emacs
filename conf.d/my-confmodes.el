;;; perfile.el --- customizations for configurations modes
;;;
;;; Time-stamp: <2019-11-07 14:44:55 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; Load auto-complete for access to ac-modes var:
(require 'auto-complete)


;; logstash-conf-mode :: edit Logstash configuration file
;; https://github.com/Wilfred/logstash-conf.el
;; -------------------------------------------

(require 'logstash-conf)

(setq logstash-indent 2)


;; fluentd-mode :: edit Fluentd configuration files
;; https://github.com/syohex/emacs-fluentd-mode
;; ------------------------------------------------

(require 'fluentd-mode)
(add-hook 'fluentd-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'fluentd-mode)

(setq fluentd-ident-level 2)


;; ssh-config-mode :: edit ~/.ssh/config
;; https://github.com/jhgorrell/ssh-config-mode-el
;; -----------------------------------------------

(require 'ssh-config-mode)
(add-hook 'ssh-config-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'ssh-config-mode)


;; conf-mode :: edit various configuration files
;; http://doc.endlessparentheses.com/Fun/conf-mode.html
;; ----------------------------------------------------

(require 'conf-mode)

(dolist
    (my-conf-ext '(("\\.conf\\'" . conf-mode)
                   ("\\.cnf\\'" . conf-mode)
                   ("\\.cf\\'" . conf-mode)
                   ("\\.config\\'" . conf-mode)))
  (add-to-list 'auto-mode-alist my-conf-ext))

(add-hook 'conf-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'conf-mode)


;; dns-mode :: emacs mode for edit BIND DNS zones
;; https://github.com/emacs-mirror/emacs/blob/master/lisp/textmodes/dns-mode.el
;; ----------------------------------------------------------------------------

(require 'dns-mode)

(dolist
    (my-dns-ext '(("\\.dns\\'" . dns-mode)
                  ("\\.zone\\'" . dns-mode)
                  ("\\.bind\\'" . dns-mode)))
  (add-to-list 'auto-mode-alist my-dns-ext))

(add-hook 'dns-mode-hook 'my-default-modes)
(setq dns-mode-soa-auto-increment-serial t)


;; apache-mode :: edit Apache HTTPD server configs
;; https://github.com/emacs-php/apache-mode
;; -----------------------------------------------

(require 'apache-mode)
(add-hook 'apache-mode-hook 'my-default-modes)


;; nginx-mode :: edit Nginx web server configs
;; https://github.com/ajc/nginx-mode
;; -------------------------------------------

(require 'nginx-mode)
(add-hook 'nginx-mode-hook 'my-default-modes)


;; junos-mode :: edit JunOS configuration files
;; https://github.com/vincentbernat/junos-mode
;; --------------------------------------------

(require 'junos-mode)

(dolist
    (my-juniper-ext '(("\\.jcfg\\'" . junos-mode)
                      ("\\.juniper\\'" . junos-mode)
                      ("\\.junos\\'" . junos-mode)
                      ("\\.jun\\'" . junos-mode)))
  (add-to-list 'auto-mode-alist my-juniper-ext))

(add-hook 'junos-mode-hook 'my-default-modes)


;; systemd-mode :: edit systemd unit files
;; https://github.com/holomorph/systemd-mode
;; -----------------------------------------

(require 'systemd)
(add-to-list 'auto-mode-alist '("\\.service\\'" . systemd-mode))
(add-hook 'systemd-mode-hook 'my-default-modes)


(provide 'my-confmodes)

;;; my-confmodes.el ends here
