;;; emacs.el --- modes for replacing Emacs main parts
;;;
;;; Commentary:

;;; Code:


;; restclient :: REST client for Emacs
;; https://github.com/pashky/restclient.el
;; ---------------------------------------

(require 'restclient)

(setq restclient-log-request t
      restclient-same-buffer-response nil
      restclient-vars-max-passes 10)


;; smart-mode-line :: colorfull mode-line for Emacs
;; https://github.com/Malabarba/smart-mode-line
;; ------------------------------------------------

(require 'smart-mode-line)

(sml/setup)


;; column-enforce-mode :: highlight text that extends beyond a certain column
;; https://github.com/jordonbiondo/column-enforce-mode
;; --------------------------------------------------------------------------

(require 'column-enforce-mode)

(setq column-enforce-column 80
      column-enforce-comments t)

(column-enforce-mode t)


;; flycheck-pos-tip :: display flycheck messages in pop-ups
;; https://github.com/flycheck/flycheck-pos-tip
;; --------------------------------------------------------

(require 'flycheck-pos-tip)

(setq flycheck-pos-tip-timeout 10
      flycheck-pos-tip-max-width 80)


(flycheck-pos-tip-mode t)


;; open-junk-file :: open a junf file to try-and-error
;; https://github.com/rubikitch/open-junk-file
;; ---------------------------------------------------

(require 'open-junk-file)

(setq open-junk-file-format "~/junk/%Y/%m/%d-%H%M%S.")


;; indent-guide :: show vertical lines to guide indentation
;; https://github.com/zk-phi/indent-guide
;; --------------------------------------------------------

(require 'indent-guide)

(setq indent-guide-char ":")

(indent-guide-global-mode)



;; flex-compile :: flexibile compile for many languages
;; https://github.com/plandes/flex-compile
;; ----------------------------------------------------

(require 'flex-compile)

(flex-compile-init)
(flex-compile-key-bindings)


;; elpl :: Emacs Lisp REPL
;; https://github.com/twlz0ne/elpl
;; -------------------------------

(require 'elpl)

(setq elpl-prompt-read-only t)


;; spaceline :: modeline from Spacemacs
;; https://github.com/TheBB/spaceline
;; ----------------------------------

;; (require 'spaceline-config)
;; (spaceline-emacs-theme)


;; emacs-dashboard :: Emacs dashboard from Spacemacs
;; https://github.com/emacs-dashboard/emacs-dashboard
;; --------------------------------------------------

(require 'dashboard)

(setq dashboard-startup-banner 'official
      dashboard-center-content t
      dashboard-show-shortcuts t
      dashboard-set-heading-icons t
      dashboard-set-file-icons t
      dashboard-set-navigator t
      dashboard-set-footer nil)

(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)
                        (registers . 5)))


;; workgroups2 :: Emacs sessions management
;; https://github.com/pashinin/workgroups2
;; ----------------------------------------

;; (require 'workgroups2)

;; (setq wg-session-file "~/.emacs.d/var/workgroups/default"
;;       wg-emacs-exit-save-behavior 'save
;;       wg-first-wg-name "default"
;;       wg-mode-line-display-on t
;;       wg-load-last-workgroup t
;;       wg-control-frames t)

;; (workgroups-mode t)

;; spacebar :: workspaces for Emacs
;; https://github.com/matthias-margush/spacebar
;; --------------------------------------------

(require 'spacebar)

(setq spacebar-window-height 2
      spacebar-active-label-format-string "(%s)"
      spacebar-perspective-label-format-string "!%s!")

(setq spacebar-keymap-prefix (kbd "C-c C-w"))


(defvar spacebar-command-map
  (let ((map (make-sparse-keymap))
        (prefix-map (make-sparse-keymap)))
    (define-key prefix-map (kbd "b") #'spacebar-switch-prev)
    (define-key prefix-map (kbd "f") #'spacebar-switch-last)
    (define-key prefix-map (kbd "x") #'spacebar-close)
    (define-key prefix-map (kbd "r") #'spacebar-rename)
    (define-key prefix-map (kbd "s") #'spacebar-switch)
    (define-key prefix-map (kbd "0") #'spacebar-switch-0)
    (define-key prefix-map (kbd "1") #'spacebar-switch-1)
    (define-key prefix-map (kbd "2") #'spacebar-switch-2)
    (define-key prefix-map (kbd "3") #'spacebar-switch-3)
    (define-key prefix-map (kbd "4") #'spacebar-switch-4)
    (define-key prefix-map (kbd "5") #'spacebar-switch-5)
    (define-key prefix-map (kbd "6") #'spacebar-switch-6)
    (define-key prefix-map (kbd "7") #'spacebar-switch-7)
    (define-key prefix-map (kbd "8") #'spacebar-switch-8)
    (define-key prefix-map (kbd "9") #'spacebar-switch-9)
    (define-key prefix-map (kbd "c") #'spacebar-open)
    (define-key prefix-map (kbd "C-c") #'spacebar-open)
    (define-key map spacebar-keymap-prefix prefix-map)
    map))

(spacebar-mode t)

(spacebar-open "default")

(when (display-graphic-p)
  (set-face-attribute 'spacebar-active nil :weight 'bold :font "Noto Sans Mono-14")
  (set-face-attribute 'spacebar-inactive nil :weight 'normal :font "Noto Sans Mono-12")
  (set-face-attribute 'spacebar-persp nil :weight 'bold :font "Noto Sans Mono-14"))


;; awesome-tab :: awesome tabs for switch between buffers
;; https://github.com/manateelazycat/awesome-tab
;; ------------------------------------------------------

(require 'awesome-tab)
(setq awesome-tab-style 'bar
      awesome-tab-label-fixed-length 16
      awesome-tab-height 12
      awesome-tab-auto-scroll-flag t
      awesome-tab-background-color "#002B36"
      awesome-tab-cycle-scope 'tabs
      awesome-tab-display-sticky-function-name t)

(add-to-list 'awesometab-hide-tabs-hooks 'term-mode-hook)

(awesome-tab-mode t)

(when (display-graphic-p)
  (set-face-attribute 'awesome-tab-selected nil :background "#829900" :foreground "black" :weight 'bold :font "Noto Sans Mono-12")
  (set-face-attribute 'awesome-tab-unselected nil :weight 'normal :font "Noto Sans Mono-10"))


;; treemacs :: tree browser mode
;; https://github.com/Alexander-Miller/treemacs
;; --------------------------------------------

(require 'treemacs)

(setq treemacs-indentation 1
      treemacs-indentation-string " "
      treemacs-show-hidden-files nil
      treemacs-file-event-delay 5000
      treemacs-file-follow-delay 0.0
      treemacs-width 40
      treemacs-persist-file "~/.emacs.d/var/treemacs/persist"
      treemacs-follow-after-init t)

(add-hook 'treemacs-select-hook (lambda () (treemacs-git-mode -1)))

;; auto-revert-mode :: automatically revert buffers
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Autorevert.html
;; -------------------------------------------------------------------------

(require 'autorevert)
(setq auto-revert-interval 10
      auto-revert-stop-on-user-input t
      auto-revert-mode t)


;; auto-save-mode :: automatically save changes
;; https://www.emacswiki.org/emacs/AutoSave
;; --------------------------------------------

(setq auto-save-list-file-prefix nil)
(auto-save-mode -1)


;; savehist :: save minibuffer history
;; https://www.emacswiki.org/emacs/SaveHist
;; ----------------------------------------

(require 'savehist)

(setq savehist-file "~/.emacs.d/var/savehist/minibuffer")

(savehist-mode t)


;; diminish :: hide minor modes from mode-line
;; https://github.com/myrjola/diminish.el
;; -------------------------------------------

(require 'diminish)

;; Hide mode names from mode-line for this modes:
(diminish 'aggressive-indent-mode)
(diminish 'eldoc-mode)
(diminish 'git-gutter-mode)
(diminish 'smartparens-mode)
(diminish 'super-save-mode)
(diminish 'volatile-highlights-mode)
(diminish 'ws-butler-mode)
(diminish 'highlight-symbol-mode)
(diminish 'global-auto-revert-mode)
(diminish 'auto-revert-mode)
(diminish 'auto-revert-tail-mode)
(diminish 'indent-guide-mode)
(diminish 'column-enforce-mode)

;; Replace mode names in mode-line fot this modes:
(diminish 'abbrev-mode "AB")
(diminish 'auto-complete-mode "AC")
(diminish 'flycheck-mode "FC")
(diminish 'hs-minor-mode "HS")
(diminish 'overwrite-mode "OW")
(diminish 'yas-minor-mode "YA")
(diminish 'guess-language-mode "GL")
(diminish 'highlight-symbol-mode "HL")
(diminish 'centered-cursor-mode "CE")
(diminish 'global-centered-cursor-mode "gCE")



;; helpful :: better *help* buffer
;; https://github.com/Wilfred/helpful
;; ----------------------------------

(require 'helpful)

(setq helpful-max-buffers 2)

(add-hook 'helpful--format-hook (lambda () (highlight-symbol-mode -1)))

;; Overwrite default help keybindings
(global-set-key (kbd "C-h f") #'helpful-callable)
(global-set-key (kbd "C-h v") #'helpful-variable)
(global-set-key (kbd "C-h k") #'helpful-key)
(global-set-key (kbd "C-c C-d") #'helpful-at-point)
(global-set-key (kbd "C-h F") #'helpful-function)
(global-set-key (kbd "C-h C") #'helpful-command)


;; reverse-im :: translate input sequences to english
;; https://github.com/a13/reverse-im.el
;; --------------------------------------------------

(require 'reverse-im)
(add-to-list 'reverse-im-input-methods "russian-computer")
(reverse-im-mode t)

(defun my-switch-language ()
  "Print input info and switch Ispell language."
  (interactive)
  (my-cycle-ispell-languages)
  (message "%s" "Input language was switched ...")
  (sit-for 2)
  (message nil))


;; mouse3 :: various actions for mouse-3 button
;; https://www.emacswiki.org/emacs/Mouse3
;; --------------------------------------------

;; (require 'mouse3)


;; popup-edit-menu :: context menu with various
;; https://github.com/debugfan/popup-edit-menu
;; --------------------------------------------

(require 'popup-edit-menu)

(setq popup-edit-menu-keep-header-flag nil
      popup-edit-menu-mode-menus-down-flag t
      popup-edit-menu-never-menu-bar-flag nil)


;; dired+ :: various enhancements for dired
;; https://www.emacswiki.org/emacs/DiredPlus
;; -----------------------------------------

(require 'dired+)
(diredp-toggle-find-file-reuse-dir t)


;; ido-mode :: interactively do things
;; https://www.emacswiki.org/emacs/InteractivelyDoThings
;; -----------------------------------------------------

(require 'ido)

(setq ido-save-directory-list-file "~/.emacs.d/var/ido/last"
      ido-everywhere t
      ido-enable-flex-matching nil
      ido-enable-regexp nil
      ido-enable-prefix nil
      ido-max-prospects 4
      ido-enable-last-directory-history t
      ido-max-work-directory-list 5
      ido-max-work-file-list 5
      ido-enable-tramp-completion nil
      ido-default-file-method 'selected-window
      ido-default-buffer-method 'selected-window
      ido-mode 'buffer
      ido-case-fold nil)

(ido-mode t)


(define-key (cdr ido-minor-mode-map-entry) [remap write-file] nil)


;; org-mode :: personal organizer and notes
;; https://orgmode.org/#docs
;; ----------------------------------------

(require 'org)
(setq org-log-done "time")
(add-hook 'org-mode-hook 'auto-fill-mode)


;; restart-emacs :: restart Emacs from Emacs
;; https://github.com/iqbalansari/restart-emacs
;; --------------------------------------------

(require 'restart-emacs)
(setq restart-emacs-restore-frames t)


;; neotree :: side-pane file manager
;; https://github.com/jaypei/emacs-neotree
;; ---------------------------------------

;; (require 'neotree)

;; (setq neo-theme 'arrow
;;       neo-window-position 'left
;;       neo-auto-indent-point nil
;;       neo-autorefresh nil
;;       neo-click-changes-root nil
;;       neo-confirm-create-directory 'off-p
;;       neo-confirm-create-file 'off-p
;;       neo-create-file-auto-open t
;;       neo-cwd-line-style 'button
;;       neo-help-echo-style 'none
;;       neo-keymap-style 'concise
;;       neo-mode-line-type 'neotree
;;       neo-show-hidden-files nil
;;       neo-smart-open nil
;;       neo-toggle-window-keep-p t
;;       neo-vc-integration nil
;;       neo-window-fixed-size t
;;       neo-window-width 30
;;       neo-hide-cursor t
;;       neo-show-slash-for-folder t)



;; (when (display-graphic-p)
;;   (set-face-attribute 'neo-header-face nil :font "Noto Sans Mono-14")
;;   (set-face-attribute 'neo-root-dir-face nil :font "Noto Sans Mono-14")
;;   (set-face-attribute 'neo-dir-link-face nil :font "Noto Sans Mono-12")
;;   (set-face-attribute 'neo-file-link-face nil :font "Noto Sans Mono-12"))


;; multi-term :: advanced version of term-mode
;; https://www.emacswiki.org/emacs/MultiTerm
;; -------------------------------------------

(require 'multi-term)

(setq multi-term-program "/bin/bash"
      multi-term-dedicated-window-height 15)

(add-hook 'term-mode-hook 'awesome-tab-local-mode)

(defun my-window-new-term ()
  "Create new window and run term inside it."
  (interactive)

  (if (string= "*terminal<1>*" (buffer-name))
      (message "Already on the terminal buffer")
    (if (get-buffer-window "*terminal<1>*")
        (switch-to-buffer-other-window "*terminal<1>*")
      ((split-window-below)
       (windmove-down)
       (multi-term)))))


(provide 'my-emacs)


;;; my-emacs.el ends here
