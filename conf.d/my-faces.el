;;; faces.el --- Emacs faces settings
;;;
;;; Time-stamp: <2019-10-16 12:59:49 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; fonts and faces
;; ---------------

(when (display-graphic-p)


  (set-face-attribute 'region nil
                      :background "#3cb371"
                      :underline t)

  ;; (set-face-attribute 'highlight-symbol-face nil
  ;;                     :background "#3cb371"
  ;;                     :foreground "#2f4f4f")

  (set-face-attribute 'cursor nil
                      :background "#A40000")

  (set-face-attribute 'fixed-pitch nil
                      :weight 'normal
                      :font "Noto Sans Mono-14" )

  (set-face-attribute 'mode-line nil
                      :weight 'bold
                      :font "Noto Sans Mono-11")

  ;; :background "#829900"
  ;; :foreground "black" )

  (set-face-attribute 'mode-line-inactive nil
                      :weight 'normal
                      :font "Noto Sans Mono-11")

  (set-face-attribute 'mode-line-buffer-id nil
                      :weight 'normal
                      :font "Noto Sans Mono-11")

  (set-face-attribute 'mode-line-emphasis nil
                      :weight 'normal
                      :font "Noto Sans Mono-11")

  (set-face-attribute 'mode-line-highlight nil
                      :weight 'normal
                      :font "Noto Sans Mono-11")

  (set-face-attribute 'header-line nil
                      :weight 'bold
                      :font "Noto Sans Mono-11")

  (set-face-attribute 'header-line-highlight nil
                      :weight 'normal
                      :font "Noto Sans Mono-11")

  (set-face-attribute 'show-paren-match nil
                      :foreground "#3cb371")

  (set-face-attribute 'minibuffer-prompt nil
                      :weight 'normal
                      :font "Noto Sans Mono-14")

  (set-face-attribute 'default nil
                      :weight 'normal
                      :font "Noto Sans Mono-14")

  (set-face-attribute 'outline-1 nil
                      :weight 'bold
                      :underline t
                      :font "Noto Sans Mono-17")

  (set-face-attribute 'outline-2 nil
                      :weight 'bold
                      :font "Noto Sans Mono-17")

  (set-face-attribute 'outline-3 nil
                      :weight 'bold
                      :underline t
                      :font "Noto Sans Mono-16")

  (set-face-attribute 'outline-4 nil
                      :weight 'bold
                      :font "Noto Sans Mono-16")

  (set-face-attribute 'outline-5 nil
                      :weight 'bold
                      :underline t
                      :font "Noto Sans Mono-15")

  (set-face-attribute 'outline-6 nil
                      :weight 'bold
                      :font "Noto Sans Mono-15")

  (set-face-attribute 'outline-7 nil
                      :weight 'bold
                      :underline t
                      :font "Noto Sans Mono-14")

  (set-face-attribute 'outline-8 nil
                      :weight 'bold
                      :font "Noto Sans Mono-14")

  ;; (set-face-attribute 'custom-face-tag nil
  ;;                     :weight 'bold
  ;;                     :underline t
  ;;                     :font "Noto Sans Mono-16")

  ;; (set-face-attribute 'custom-group-tag nil
  ;;                     :weight 'bold
  ;;                     :underline t
  ;;                     :font "Noto Sans Mono-16")

  ;; (set-face-attribute 'custom-group-tag-1 nil
  ;;                     :weight 'bold
  ;;                     :underline t
  ;;                     :font "Noto Sans Mono-16")

  ;; (set-face-attribute 'custom-variable-tag nil
  ;;                     :weight 'bold
  ;;                     :font "Noto Sans Mono-16")
  )


(provide 'my-faces)


;;; my-faces.el ends here
