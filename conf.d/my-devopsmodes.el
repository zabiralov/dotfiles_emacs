;;; perfile.el --- customizations for devops modes
;;;
;;; Time-stamp: <2019-12-17 01:11:34 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; Load auto-complete for access to ac-modes var:
(require 'auto-complete)

;; terraform-node :: mode for edit terraform manifests
;; https://github.com/syohex/emacs-terraform-mode
;; ---------------------------------------------------

(require 'terraform-mode)
(add-to-list 'auto-mode-alist '("\\.tf\\'" . terraform-mode))
(add-hook 'terraform-mode-hook 'my-default-modes)

(setq terraform-indent-level 2)


;; gitlab-ci-mode :: edit GitLab CI pipelines
;; https://github.com/emacsmirror/gitlab-ci-mode
;; ---------------------------------------------

(require 'gitlab-ci-mode)
(add-to-list 'auto-mode-alist '("\\.gitlab-ci.yml\\'" . gitlab-ci-mode))
(add-hook 'gitlab-ci-mode-hook 'my-default-modes)

(defvar gitlab-ci-tokens-file "~/.emacs.d/secrets/gitlab-tokens.el")
(load gitlab-ci-tokens-file)


;; rpm-spec-mode :: edit RPM spec files
;; https://github.com/stigbjorlykke/rpm-spec-mode
;; ----------------------------------------------

(require 'rpm-spec-mode)
(add-to-list 'auto-mode-alist '("\\.spec\\'" . rpm-spec-mode))
(add-hook 'sql-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'rpm-spec-mode)


;; json-mode :: edit JSON files
;; https://github.com/joshwnj/json-mode
;; ------------------------------------

(require 'json-mode)
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))
(add-hook 'json-mode-hook 'my-default-modes)
(add-to-list 'ac-modes 'json-mode)


;; ansible :: edit Ansible playbooks
;; https://github.com/k1LoW/emacs-ansible
;; --------------------------------------

(require 'ansible)
(require 'ansible-doc)
(add-hook 'yaml-mode-hook '(lambda () (ansible t)))
(add-hook 'yaml-mode-hook '(lambda () (ansible-doc-mode t)))


;; dockerfile-mode :: edit Dockerfiles
;; https://github.com/spotify/dockerfile-mode
;; ------------------------------------------

(require 'dockerfile-mode)
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))
(add-to-list 'dockerfile-mode-hook 'my-default-modes)


;; docker-compose-mode :: edit docker-compose manifests
;; https://github.com/meqif/docker-compose-mode
;; ----------------------------------------------------

(require 'docker-compose-mode)
(add-to-list 'auto-mode-alist '("docker\\-compose\\.yml\\'" . docker-compose-mode))
(add-to-list 'docker-compose-mode-hook 'my-default-modes)


(provide 'my-devopsmodes)

;;; my-devopsmodes.el ends here
