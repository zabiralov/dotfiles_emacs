;;; editing.el --- customization for Emacs edit expirience
;;;
;;; Time-stamp: <2019-12-03 15:01:56 azabiralov>
;;;
;;; Commentary:

;;; Code:

;; multiple-cursors :: edit text with multiple cursrors
;; ----------------------------------------------------
;; https://github.com/magnars/multiple-cursors.el

(require 'multiple-cursors)
(require 'mc-extras)
(require 'phi-search)

(global-set-key (kbd "C-s") 'phi-search)
(global-set-key (kbd "C-r") 'phi-search-backward)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-/") 'mc/mark-all-like-this)

(global-unset-key (kbd "<C-down-mouse-1>"))
(global-unset-key (kbd "<C-up-mouse-1>"))

(global-set-key (kbd "C-<mouse-1>") 'mc/add-cursor-on-click)


;; easy-pg :: Emacs frontend for GnuPG
;; https://www.gnu.org/software/emacs/manual/html_mono/epa.html
;; ------------------------------------------------------------

(require 'epa-file)

(setq epa-file-select-keys "none"
      epa-file-cache-passphrase-for-symmetric-encryption t
      epa-pinentry-mode 'loopback
      epa-popup-info-window nil)


;; centered-cursor-mode :: put cursor always on center of window
;; https://github.com/andre-r/centered-cursor-mode.el
;; -------------------------------------------------------------

(require 'centered-cursor-mode)

(setq ccm-recenter-at-end-of-file t
      ccm-step-delay 0.0)


;; TRAMP :: transparent remote access
;; https://www.emacswiki.org/emacs/TrampMode
;; -----------------------------------------

(require 'tramp)

(setq tramp-default-method "scp"
      tramp-histfile-override "~/.emacs.d/var/tramp/history")


;; guess-language :: automatic human language detection
;; https://github.com/tmalsburg/guess-language.el
;; ----------------------------------------------------

;; (require 'guess-language)

;; (setq guess-language-languages '(en ru)
;;       guess-language-min-paragraph-length 35)


;; ispell-mode :: spell checking
;; https://www.emacswiki.org/emacs/InteractiveSpell
;; ------------------------------------------------

(require 'ispell)

(setq ispell-choices-win-default-height 4
      ispell-program-name "/usr/bin/hunspell"
      ispell-dictionary "russian")


;; flyspell-mode :: spell checking on-the-fly
;; https://www.emacswiki.org/emacs/InteractiveSpell
;; ------------------------------------------------

(require 'flyspell)

(setq flyspell-highlight-flag t
      flyspell-mark-duplications-flag t
      flyspell-delay 3
      flyspell-persistent-highlight t
      flyspell-issue-welcome-flag t
      flyspell-issue-message-flag t
      flyspell-mode-line-string " Fl"
      flyspell-large-region 10000)


;; hightlight-symbol :: automatic and manual symbol highlighting
;; https://github.com/nschum/highlight-symbol.el
;; -------------------------------------------------------------

(require 'highlight-symbol)

(setq highlight-symbol-idle-delay 300
      highlight-symbol-on-navigation-p nil
      highlight-symbol-foreground-color "#2f4f4f"
      highlight-symbol-colors '("#3cb371"))


;; volatile-highlists :: highlight recent text operations
;; https://github.com/k-talo/volatile-highlights.el
;; ------------------------------------------------------

(require 'volatile-highlights)
(volatile-highlights-mode t)
(setq Vhl/highlight-zero-width-ranges t)


;; flycheck-mode :: check sources on-the-fly
;; https://github.com/flycheck/flycheck
;; ------------------------------------

(require 'flycheck)
(global-flycheck-mode t)



;; yasnippet-mode :: snippets for Emacs
;; https://github.com/joaotavora/yasnippet
;; ---------------------------------------

(require 'yasnippet)
(require 'yasnippet-snippets)
(require 'yasnippet-classic-snippets)
(require 'common-lisp-snippets)
(require 'clojure-snippets)
(require 'vala-snippets)
(require 'go-snippets)

(setq yas-snippet-dirs '("~/.emacs.d/snippets")
      yas-indent-line "fixed"
      yas-trigger-symbol ">>>"
      yas-also-indent-empty-lines t
      yas-choose-keys-first t)

(yas-global-mode 1)

(global-set-key (kbd "M-<tab>") 'yas-expand)


;; yatemplate :: template new files with yasnippet and auto-insert
;; https://github.com/mineo/yatemplate
;; ---------------------------------------------------------------

(require 'yatemplate)
(require 'autoinsert)

(setq auto-insert-alist nil
      yatemplate-dir "~/.emacs.d/templates"
      yatemplate-separator ":")


;; whitespace-mode :: show whitespace characters
;; https://www.emacswiki.org/emacs/WhiteSpace
;; ---------------------------------------------

(require 'whitespace)
(global-set-key (kbd "C-x w") 'whitespace-mode)


;; ideit-mode :: edit multiple regions
;; https://github.com/victorhge/iedit
;; -----------------------------------

(require 'iedit)


;; aggressive-indent-mode :: aggressive automatic code indent
;; https://github.com/Malabarba/aggressive-indent-mode
;; ----------------------------------------------------------

(require 'aggressive-indent)
(global-aggressive-indent-mode t)


;; smartparens :: auto-complete for parens
;; https://github.com/Fuco1/smartparens
;; ---------------------------------------

(require 'smartparens-config)
(smartparens-global-mode t)
;; (smartparens-strict-mode t)


;; git-gutter :: show diffs between git commits
;; https://github.com/syohex/emacs-git-gutter
;; --------------------------------------------

(require 'git-gutter)
(global-git-gutter-mode t)
(setq git-gutter:update-interval 1
      git-gutter:added-sign "++"
      git-gutter:deleted-sign "--"
      git-gutter:modified-sign "**")


;; rotate-text :: rotate text at point
;; https://www.emacswiki.org/emacs/RotateText
;; ------------------------------------------

(require 'rotate-text)

(setq rotate-text-rotations nil)

(dolist
    (my-rotate
     '(("Y" "N")
       ("y" "n")
       ("yes" "no")
       ("Yes" "No")
       ("YES" "NO")
       ("t" "nil")
       ("true" "false")
       ("True" "False")
       ("TRUE" "FALSE")
       ("enable" "disable")
       ("Enable" "Disable")
       ("ENABLE" "DISABLE")
       ("on" "off")
       ("On" "Off")
       ("ON" "OFF")
       ("1" "0")))
  (add-to-list 'rotate-text-rotations my-rotate))

(global-set-key (kbd "S-<SPC>") 'rotate-word-at-point)


;; ws-butter :: trim spaces from end of line
;; https://github.com/lewang/ws-butler
;; -----------------------------------------

(require 'ws-butler)


;; rainbow-delimeters :: colorful delimeters for code
;; https://github.com/Fanael/rainbow-delimiters
;; --------------------------------------------------

(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)


;; show-paren-mode :: show paren characters
;; https://www.emacswiki.org/emacs/ShowParenMode
;; ---------------------------------------------

(require 'paren)
(show-paren-mode t)
(setq show-paren-style 'expression
      show-paren-delay 0)


;; autocomplete-mode :: automatic completion
;; https://github.com/auto-complete/auto-complete
;; ----------------------------------------------

(require 'auto-complete)
(ac-config-default)
(global-auto-complete-mode t)

(setq ac-delay 0.1
      ac-auto-show-menu 1.0
      ac-expand-on-auto-complete t
      ac-stop-flymake-on-completing t
      ac-flycheck-poll-completion-end-interval 1.0
      ac-use-quick-help nil
      ac-menu-height 5
      ac-ignore-case t
      ac-max-width nil
      ac-comphist-file "~/.emacs.d/var/ac/history")


;; hs-mode (hide-show) :: hide and show text blocks
;; https://www.emacswiki.org/emacs/HideShow
;; ------------------------------------------------

(require 'hideshow)

(defvar hs-special-modes-alist
  (mapcar 'purecopy
          '((c-mode "{" "}" "/[*/]" nil nil)
            (conf-mode "{" "}" "/[*/]" nil nil)
            (go-mode "{" "}" "/[*/]" nil nil)
            (latex-mode "\begin" "\end" "/[*/]" nil nil)
            (perl-mode "{" "}" "/[*/]" nil nil)
            (sh-mode "{" "}" "/[*/]" nil nil)
            (vala-mode "{" "}" "/[*/]" nil nil))))

(setq hs-hide-comments-when-hiding-all t
      hs-isearch-open t)


;; browse-kill-ring :: interactively insert items from kill-ring
;; https://github.com/browse-kill-ring/browse-kill-ring
;; -------------------------------------------------------------

(require 'browse-kill-ring)
(setq browse-kill-ring-display-style 'separated
      browse-kill-ring-recenter nil
      browse-kill-ring-separator "+++++++++++++++"
      browse-kill-ring-highlight-current-entry t)

(global-set-key (kbd "C-x p") 'browse-kill-ring)


;; header2 :: templates for new-created files
;; https://www.emacswiki.org/emacs/header2.el
;; ------------------------------------------

(require 'header2)


;; time-stamp :: automatically insert time stamps
;; https://gnu.org/s/emacs/manual/html_node/emacs/Time-Stamps.html
;; ---------------------------------------------------------------

(require 'time-stamp)
(setq time-stamp-line-limit 50)

(add-hook 'before-save-hook 'time-stamp)


(provide 'my-editing)

;;; my-editing.el ends here
