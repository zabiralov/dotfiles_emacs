;;; functions.el --- custom ELisp functions
;;;
;;; Time-stamp: <2019-11-08 14:19:45 azabiralov>
;;;
;;; Commentary:

;;; Code:


;; default modes enable via add-hook
;; ---------------------------------

(defun my-default-modes ()
  "List of buffer-local modes enabled by default."
  (interactive)
  (highlight-symbol-mode t)
  (hs-minor-mode t)
  (goto-address-prog-mode t)
  (ws-butler-mode t))


;; custom-functions
;; ----------------

(defvar my-ispell-languages-ring nil "Languages ring for Ispell.")

(require 'validate)

(let ((languages '("en_US" "ru_RU")))
  (validate-setq my-ispell-languages-ring (make-ring (length languages)))
  (dolist (elem languages) (ring-insert my-ispell-languages-ring elem)))

(defun my-cycle-ispell-languages ()
  "Cycle switch between ispell-checking input languages."
  (interactive)
  (let ((language (ring-ref my-ispell-languages-ring -1)))
    (ring-insert my-ispell-languages-ring language)
    (ispell-change-dictionary language)))


(defun my-fill-paragraph ()
  "Fill paragraph by width."
  (interactive)
  (fill-paragraph 1))


(defun my-switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (switch-to-buffer-other-window (other-buffer (current-buffer) 1)))


(defun my-forward-delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument ARG, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (delete-region
   (point)
   (progn
     (forward-word arg)
     (point))))


(defun my-backward-delete-word (arg)
  "Delete characters backward until encountering the beginning of a word.
With argument ARG, do this that many times.
This command does not push text to `kill-ring'."
  (interactive "p")
  (my-delete-word (- arg)))


(defun my-forward-delete-line ()
  "Delete text from current position to end of line char.
This command does not push text to `kill-ring'."
  (interactive)
  (delete-region
   (point)
   (progn (end-of-line 1) (point)))
  (delete-char 1))


(defun my-backward-delete-line ()
  "Delete text between the beginning of the line to the cursor position.
This command does not push text to `kill-ring'."
  (interactive)
  (let (p1 p2)
    (setq p1 (point))
    (beginning-of-line 1)
    (setq p2 (point))
    (delete-region p1 p2)))


(defun my-rename-file-and-buffer (arg)
  "Renames both current buffer and file it's visiting to ARG."
  (interactive "sNew file and buffer name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer arg)
          (message "A buffer named '%s' already exists!" arg)
        (progn
          (rename-file filename arg 1)
          (rename-buffer arg)
          (set-visited-file-name arg)
          (set-buffer-modified-p nil))))))


(defvar my-eshell-buffer-displayedp nil
  "Shows if eshell buffer is displayed.")

(setq my-eshell-buffer-displayedp nil)

(defvar my-eshell-buffer "*my-eshell*"
  "Name of eshell buffer.")

(setq my-eshell-buffer "*my-eshell*")


(defun my-eshell-start ()
  "Start eshell."
  (interactive)
  (generate-new-buffer my-eshell-buffer)
  (popwin:pop-to-buffer my-eshell-buffer)
  (setq my-eshell-buffer-displayedp 't)
  (eshell-mode))


(defun my-eshell-toggle ()
  "Toggle eshell buffer."
  (interactive)
  (if my-eshell-buffer-displayedp
      (progn
        (popwin:close-popup-window)
        (setq my-eshell-buffer-displayedp nil))
    (progn
      (ignore-errors (popwin:display-buffer my-eshell-buffer))
      (setq my-eshell-buffer-displayedp 't))))


(defun my-packages-new-frame ()
  "Start Emacs package manager in separate frame."
  (interactive)
  (select-frame (make-frame))
  (spacebar-open "packages")
  (package-list-packages))


(defun my-packages ()
  "Start Emacs package manager in current frame."
  (interactive)
  (spacebar-open "packages")
  (package-list-packages))


(defun my-elpl-new-frame ()
  "Start ELPL in separate frame."
  (interactive)
  (select-frame (make-frame))
  (spacebar-open "elpl")
  (elpl))


(defun my-telega-new-frame ()
  "Start telega in separate frame."
  (interactive)
  (select-frame (make-frame))
  (spacebar-open "telega")
  (telega t))


(defun my-gnus-new-frame ()
  "Start Gnus in separate frame."
  (interactive)
  (select-frame (make-frame))
  (spacebar-open "gnus")
  (gnus))


(defun my-dired-new-frame ()
  "Start Dired in separate frame."
  (interactive)
  (select-frame (make-frame))
  (spacebar-open "dired")
  (dired-single-buffer "~"))


(defun my-dired ()
  "Start Dired in current frame."
  (interactive)
  (spacebar-open "dired")
  (dired-single-buffer "~"))


(require 'scratch)

(defun my-restclient ()
  "Start Restclient in separate frame."
  (interactive)
  (select-frame (make-frame))
  (spacebar-open "restclient")
  (scratch 'restclient-mode)
  (delete-other-windows))


(defun my-emacs-startup ()
  "My custom Emacs Startup commands."
  (interactive)

  ;; Setup window layout
  ;; (my-eshell-start)
  (treemacs)
  ;; (other-window 1)
  (treemacs-RET-action)
  (treemacs-resize-icons 18)

  (dashboard-setup-startup-hook)

  ;; Start Emacs as daemon, if already not running
  (require 'server)
  (unless (server-running-p)
    (server-start)))


(provide 'my-functions)

;;; my-functions.el ends here
