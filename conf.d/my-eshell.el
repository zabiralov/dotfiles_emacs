;;; my-eshell.el --- configuration for Eshell
;;;
;;; Commentary:

;;; Code:

(require 'eshell)
(require 'esh-mode)
(require 'em-banner)
(require 'em-basic)
(require 'em-cmpl)
(require 'em-dirs)
(require 'em-glob)
(require 'em-hist)
(require 'em-ls)
(require 'em-pred)
(require 'em-prompt)
(require 'em-smart)
(require 'em-term)
(require 'em-unix)
(require 'em-xtra)


(setq eshell-command-completions-alist
      '(( "zathura" . "\\.pdf\\'")
        ("gcc"      . "\\.[Cc]\\([Cc]\\|[Pp][Pp]\\)?\\'")
        ("g++"      . "\\.[Cc]\\([Cc]\\|[Pp][Pp]\\)?\\'")))


(setq eshell-banner-message "\n")
(setq eshell-prompt-regexp "> ")
(setq eshell-prompt-function (lambda() (concat "> ")))

(setq eshell-directory-name "~/.emacs.d/var/eshell")
(setq eshell-last-dir-ring-size 100)
(setq eshell-last-dir-unique nil)
(setq eshell-glob-show-progress nil)
(setq eshell-history-size 1000000)
(setq eshell-hist-ignoredups t)
(setq eshell-where-to-jump 'end)
(setq eshell-plain-grep-behavior t)
(setq eshell-plain-diff-behavior t)
(setq eshell-plain-locate-behavior t)
(setq eshell-rm-removes-directories t)
(setq eshell-rm-interactive-query t)
(setq eshell-mv-overwrite-files nil)
(setq eshell-scroll-to-bottom-on-input "this")
(setq eshell-buffer-maximum-lines 100000)
(setq eshell-cd-shows-directory t)
(setq eshell-list-files-after-cd t)



(provide 'my-eshell)


;;; my-eshell.el ends here
