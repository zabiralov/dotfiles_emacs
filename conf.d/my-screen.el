;;; my-screen.el --- configuration for Emacs 'screen' modes
;;;
;;; Commentary:

;;; Code:


;; kubernetes-el :: Kubernetes magit-like client
;; https://github.com/chrisbarrett/kubernetes-el
;; ---------------------------------------------


(require 'kubernetes)

(provide 'my-screen)


;;; my-screen.el ends here
